<?php

namespace Database\Factories;


use App\Domain\Location\Entities\City;
use App\Domain\Location\Entities\Province;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = City::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->city(),
            'province_id' => $this->faker->randomElement(Province::pluck('id')),
            'active' => $this->faker->boolean(),
            'created_at' => now(),
            'updated_at' => now(),
        ];

    }
}
