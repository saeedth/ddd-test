<?php

namespace Database\Factories;

use App\Domain\Demand\Entities\Request;
use App\Domain\Location\Entities\Area;
use App\Domain\Seprator\Entities\Attribute;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class Requests extends Seeder
{
    public function run()
    {
        $faker = Factory::create();
        // Schema::disableForeignKeyConstraints();
        $areas = Area::query()->pluck('id')->toArray();

        // dd($areas);

        for ($i = 0; $i < 20; $i++) {
            $request = Request::factory()->create();
            // dd($request);

            // $proposalIds=Proposal::query()->where('category_id',$request->category_id)->pluck('id')->toArray();

            // sizeof(Arr::random($proposalIds, 1)) ? $request->proposals()->attach(Arr::random($proposalIds,1)) :null;


            $request->areas()->attach(Arr::random($areas, 2));

            $attributesBooleanTypes = Attribute::query()->where('type', 3)->where('category_id', $request->category_id)->pluck('id')->toArray();
            // dd($attributesBooleanTypes);
            $request->booleans()->attach($attributesBooleanTypes, ['checked' => random_int(0, 1)]);

            $attributesNumberTypes = Attribute::query()->where('type', 1)->where('category_id', $request->category_id)->pluck('id')->toArray();
            $request->numbers()->attach($attributesNumberTypes, ['number' => random_int(1, 100)]);

            $attributesPeriodTypes = Attribute::query()->where('type', 2)->where('category_id', $request->category_id)->pluck('id')->toArray();
            $request->periods()->attach($attributesPeriodTypes, ['from' => random_int(1, 100), 'to' => random_int(1, 100)]);
        }
    }
}
