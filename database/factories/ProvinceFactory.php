<?php

namespace Database\Factories;


use App\Domain\Location\Entities\Province;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProvinceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Province::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->state(),
            'tell_prefix' => '0' . $this->faker->numberBetween(11, 99),
            'active' => $this->faker->boolean(),
            'created_at' => now(),
            'updated_at' => now(),
        ];

    }
}
