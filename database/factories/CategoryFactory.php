<?php

namespace Database\Factories;

use App\Domain\Seprator\Entities\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(4),
            'active' => $this->faker->boolean(),
            'description' => $this->faker->realText(100, 2),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
