<?php

namespace Database\Factories;

use App\Domain\Account\Entities\User;
use App\Domain\Demand\Entities\Request;
use App\Domain\Seprator\Entities\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class RequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Request::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(30),
            'category_id' => $this->faker->randomElement(Category::query()->pluck('id')),
            'user_id' => $this->faker->randomElement(User::query()->pluck('id')),
            'status' => $this->faker->randomElement([1, 2, 3]),
            'tracking_code' => $this->faker->numberBetween(1000000, 9000000),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
