<?php

namespace Database\Factories;

use App\Domain\Seprator\Entities\Attribute;
use App\Domain\Seprator\Entities\Category;
use App\Domain\Seprator\Entities\Unit;
use Illuminate\Database\Eloquent\Factories\Factory;

class AttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(4),
            'category_id' => $this->faker->randomElement(Category::pluck('id')),
            'unit_id' => $this->faker->randomElement(Unit::pluck('id')),
            'active' => $this->faker->boolean(),
            'filter' => $this->faker->boolean(),
            'sort' => $this->faker->numberBetween(1, 100),
            'type' => $this->faker->randomElement([1, 2, 3]),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
