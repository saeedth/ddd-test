<?php

namespace Database\Factories;

use App\Domain\Location\Entities\Area;
use App\Domain\Location\Entities\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class AreaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Area::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->country(),
            'city_id' => $this->faker->randomElement(City::pluck('id')),
            'aliases' => $this->faker->words(3, false),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
