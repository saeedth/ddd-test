<?php

namespace Database\Factories;

use App\Domain\Account\Entities\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'admin',
            'editable' => false,
            'guard_name' => 'api',
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
