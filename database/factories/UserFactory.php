<?php

namespace Database\Factories;

use App\Domain\Account\Entities\User;
use App\Domain\Location\Entities\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'mobile' => $this->faker->mobileNumber(),
            'type' => $this->faker->randomElement([1, 2]),
            'city_id' => $this->faker->randomElement(City::pluck('id')),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
