<?php

namespace Database\Factories;

use App\Domain\Seprator\Entities\Unit;
use Illuminate\Database\Eloquent\Factories\Factory;

class UnitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Unit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'englishName' => 'english ' . $this->faker->word(),
            'persianName' => 'persian ' . $this->faker->word(),
            "Active" => $this->faker->boolean(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
