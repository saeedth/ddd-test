<?php

namespace Database\Seeders;

use App\Domain\Account\Entities\Permission as EntitiesPermission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PermissionsTableSeeder extends Seeder
{
    private array $domains = [
        'location' => [
            'area',
            'city',
            'province',
        ],
        'account' => [
            'role',
            'user',
        ],
        'demand' => [
            'request'
        ],
        'separator' => [
            'attribute',
            'category',
            'unit',
        ],
        'supply' => [
            'proposal',
        ]
    ];

    private array $permissionAction = [
        'show',
        'index',
        'create',
        'delete',
        'update',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        foreach ($this->domains as $domain => $entities) {
            foreach ($entities as $entity) {
                foreach ($this->permissionAction as $action) {
                    EntitiesPermission::create(['name' => "${domain}.${entity}.${action}"]);
                }
            }
        }

        DB::commit();
    }
}
