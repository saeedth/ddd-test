<?php

namespace Database\Seeders;

use App\Domain\Account\Entities\Permission;
use App\Domain\Account\Entities\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::beginTransaction();

        $permissions = Permission::query()->pluck('id')->toArray();

        $role = Role::factory()->create();

        $role->permissions()->attach($permissions);

        DB::commit();
    }
}
