<?php

namespace Database\Seeders;

use App\Domain\Account\Entities\Role;
use App\Domain\Account\Entities\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::beginTransaction();

        $user = User::query()->create([
            'first_name' => null,
            'last_name' => null,
            'mobile' => 9392142022
        ]);

        $adminsRole = Role::query()->first()->pluck('id');

        $user->roles()->attach($adminsRole);

        DB::commit();
    }
}
