<?php


use App\Domain\Separator\Enums\AttributeTypeEnum;

return AttributeTypeEnum::getLocalizeFaDescription();
