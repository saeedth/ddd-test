<?php

// use App\Http\Controllers\Api\Business\DonateController;
// use App\Http\Controllers\Api\Business\TransactionController;
use App\Http\Controllers\Web\Front\ApplicationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', [ApplicationController::class, 'index'])->where('any', '^(?!api).*');

// Route::get('/transaction/payment', [TransactionController::class, 'payment']);

// Route::get('/transaction/verify', [DonateController::class, 'index']);

