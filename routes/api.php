<?php

use App\Http\Controllers\Api\Account\PermissionController;
use App\Http\Controllers\Api\Account\ProfileController;
use App\Http\Controllers\Api\Account\RoleController;
use App\Http\Controllers\Api\Account\SessionController;
use App\Http\Controllers\Api\Account\UserController;
use App\Http\Controllers\Api\Auth\CheckExistsController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\LogoutController;
use App\Http\Controllers\Api\Auth\RefreshTokenController;
use App\Http\Controllers\Api\Business\AdvisorController;
use App\Http\Controllers\Api\Business\DonateController;
use App\Http\Controllers\Api\Business\TransactionController;
use App\Http\Controllers\Api\Demand\RequestController;
use App\Http\Controllers\Api\Location\AreaController;
use App\Http\Controllers\Api\Location\CityController;
use App\Http\Controllers\Api\Location\ProvinceController;
use App\Http\Controllers\Api\separator\AttributeController;
use App\Http\Controllers\Api\separator\CategoryController;
use App\Http\Controllers\Api\separator\UnitController;
use App\Http\Controllers\Api\Supply\ProposalController;
use Dingo\Api\Facade\API;
use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the 'api' middleware group. Enjoy building your API!
|
*/

/**
 * @var $api Router
 */

API::version('v1', function ($api) {

    API::group(['prefix' => 'auth', 'middleware' => ['guest', 'throttle:20']], function ($api) {
        API::get('check_exists', [CheckExistsController::class, 'handle']);
        API::post('login', [LoginController::class, 'handle']);
        API::post('retry', [LoginController::class, 'resend']);
        API::post('verify', [LoginController::class, 'verify']);
        API::post('logout', [LogoutController::class, 'logout'])->middleware('api.auth');

    });

    API::get('auth/refreshToken', [RefreshTokenController::class, 'refreshToken'])->middleware('auth:sanctum');
    // profile route
    API::group(['prefix' => 'user', 'middleware' => ['api.auth']], function ($api) {
        API::put('edit/profile', [ProfileController::class, 'edit']);
        API::post('add/avatar', [ProfileController::class, 'addAvatar']);
        API::get('profile', [ProfileController::class, 'show']);
    });
    //users route
    API::group(['prefix' => 'users', 'middleware' => ['api.auth']], function ($api) {
        API::get('/', [UserController::class, 'index'])->middleware(['role_or_permission:account.user.index,sanctum']);
        API::get('/{id}', [UserController::class, 'show'])->whereNumber('id')->middleware(['role_or_permission:account.user.show,sanctum']);
        API::post('/', [UserController::class, 'create'])->middleware(['role_or_permission:account.user.create,sanctum']);
        API::put('/{id}', [UserController::class, 'update'])->whereNumber('id')->middleware(['role_or_permission:account.user.update,sanctum']);
        API::delete('/{id}', [UserController::class, 'delete'])->whereNumber('id')->middleware(['role_or_permission:account.user.delete,sanctum']);
    });
    // province routes
    API::group(['prefix' => 'provinces', 'middleware' => ['api.auth']], function ($api) {
        API::get('/', [ProvinceController::class, 'index'])->middleware(['role_or_permission:location.province.index,sanctum']);
        API::get('/{id}', [ProvinceController::class, 'show'])->middleware(['role_or_permission:location.province.show,sanctum'])->whereNumber('id');
        API::post('/', [ProvinceController::class, 'create'])->middleware(['role_or_permission:location.province.create,sanctum']);
        API::put('/{id}', [ProvinceController::class, 'update'])->middleware(['role_or_permission:location.province.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [ProvinceController::class, 'delete'])->middleware(['role_or_permission:location.province.delete,sanctum'])->whereNumber('id');
    });

    // cities routes

    API::group(['prefix' => 'cities', 'middleware' => ['api.auth']], function ($api) {
        API::get('/', [CityController::class, 'index'])->middleware(['role_or_permission:location.city.index,sanctum']);
        API::get('/{id}', [CityController::class, 'show'])->middleware(['role_or_permission:location.city.show,sanctum'])->whereNumber('id');
        API::post('/', [CityController::class, 'create'])->middleware(['role_or_permission:location.city.create,sanctum']);
        API::put('/{id}', [CityController::class, 'update'])->middleware(['role_or_permission:location.city.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [CityController::class, 'delete'])->middleware(['role_or_permission:location.city.delete,sanctum'])->whereNumber('id');
    });

    // areas routes

    API::group(['prefix' => 'areas', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [AreaController::class, 'index'])->middleware(['role_or_permission:location.area.index,sanctum']);
        API::get('/{id}', [AreaController::class, 'show'])->middleware(['role_or_permission:location.area.show,sanctum'])->whereNumber('id');
        API::post('/', [AreaController::class, 'create'])->middleware(['role_or_permission:location.area.create,sanctum']);
        API::put('/{id}', [AreaController::class, 'update'])->middleware(['role_or_permission:location.area.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [AreaController::class, 'delete'])->middleware(['role_or_permission:location.area.delete,sanctum'])->whereNumber('id');
    });


    // category routes

    API::group(['prefix' => 'categories', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [CategoryController::class, 'index'])->middleware(['role_or_permission:separator.category.index,sanctum']);
        API::get('/{id}', [CategoryController::class, 'show'])->middleware(['role_or_permission:separator.category.show,sanctum'])->whereNumber('id');
        API::post('/', [CategoryController::class, 'create'])->middleware(['role_or_permission:separator.category.create,sanctum']);;
        API::put('/{id}', [CategoryController::class, 'update'])->middleware(['role_or_permission:separator.category.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [CategoryController::class, 'delete'])->middleware(['role_or_permission:separator.category.delete,sanctum'])->whereNumber('id');
    });

    // attribute routes

    API::group(['prefix' => 'attributes', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [AttributeController::class, 'index'])->middleware(['role_or_permission:separator.attribute.index,sanctum']);
        API::get('/{id}', [AttributeController::class, 'show'])->middleware(['role_or_permission:separator.attribute.show,sanctum'])->whereNumber('id');
        API::post('/', [AttributeController::class, 'create'])->middleware(['role_or_permission:separator.attribute.create,sanctum']);
        API::put('/{id}', [AttributeController::class, 'update'])->middleware(['role_or_permission:separator.attribute.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [AttributeController::class, 'delete'])->middleware(['role_or_permission:separator.attribute.delete,sanctum'])->whereNumber('id');
    });
    // request routes

    API::group(['prefix' => 'requests', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [RequestController::class, 'index'])->middleware(['role_or_permission:demand.request.index,sanctum']);
        API::get('/{id}', [RequestController::class, 'show'])->middleware(['role_or_permission:demand.request.show,sanctum'])->whereNumber('id')->name('requests.show');
        API::post('/', [RequestController::class, 'create'])->middleware(['role_or_permission:demand.request.create,sanctum']);
        API::put('/{id}', [RequestController::class, 'update'])->middleware(['role_or_permission:demand.request.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [RequestController::class, 'delete'])->middleware(['role_or_permission:demand.request.delete,sanctum'])->whereNumber('id');
        // store some proposal for special request
        API::post('/{id}/assign-proposals', [RequestController::class, 'sendProposalsToSpecialRequest'])->whereNumber('id')->name('requests.assign-proposals');
    });

    // units routes

    API::group(['prefix' => 'units', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [UnitController::class, 'index'])->middleware(['role_or_permission:separator.unit.index,sanctum']);
        API::get('/{id}', [UnitController::class, 'show'])->middleware(['role_or_permission:separator.unit.show,sanctum'])->whereNumber('id');
        API::post('/', [UnitController::class, 'create'])->middleware(['role_or_permission:separator.unit.create,sanctum']);
        API::put('/{id}', [UnitController::class, 'update'])->middleware(['role_or_permission:separator.unit.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [UnitController::class, 'delete'])->middleware(['role_or_permission:separator.unit.delete,sanctum'])->whereNumber('id');
    });

    // roles routes
    API::group(['prefix' => 'roles', 'middleware' => ['api.auth']], function ($api) {
        API::get('/', [RoleController::class, 'index'])->middleware(['role_or_permission:account.role.index,sanctum']);
        API::get('/{id}', [RoleController::class, 'show'])->middleware(['role_or_permission:account.role.show,sanctum'])->whereNumber('id');
        API::post('/', [RoleController::class, 'create'])->middleware(['role_or_permission:account.role.create,sanctum']);
        API::put('/{id}', [RoleController::class, 'update'])->middleware(['role_or_permission:account.role.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [RoleController::class, 'delete'])->middleware(['role_or_permission:account.role.delete,sanctum'])->whereNumber('id');
    });

    // permissions routes
    API::group(['prefix' => 'permissions', 'middleware' => ['api.auth']], function ($api) {
        API::get('/', [PermissionController::class, 'index']);
        API::get('/{id}', [PermissionController::class, 'show'])->whereNumber('id');
        API::post('/', [PermissionController::class, 'create']);
        API::put('/{id}', [PermissionController::class, 'update'])->whereNumber('id');
        API::delete('/{id}', [PermissionController::class, 'delete'])->whereNumber('id');
    });

    // proposal routes
    API::group(['prefix' => 'proposals', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [ProposalController::class, 'index'])->middleware(['role_or_permission:supply.proposal.index,sanctum']);
        API::get('/{id}', [ProposalController::class, 'show'])->middleware(['role_or_permission:supply.proposal.show,sanctum'])->whereNumber('id')->name('proposal.show');
        API::post('/', [ProposalController::class, 'create'])->middleware(['role_or_permission:supply.proposal.create,sanctum']);
        API::put('/{id}', [ProposalController::class, 'update'])->middleware(['role_or_permission:supply.proposal.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [ProposalController::class, 'delete'])->middleware(['role_or_permission:supply.proposal.delete,sanctum'])->whereNumber('id');
        //add proposal image
        API::post('/add/Image', [ProposalController::class, 'addProposalImageToGallery']);
        // send proposal to special user
        API::post('/send-proposal-to-user', [ProposalController::class, 'sendProposalToSpecialUser']);
    });
    // /advisor

    API::group(['prefix' => 'advisors', 'middleware' => ['api.auth']], function ($api) {

        API::get('/', [AdvisorController::class, 'index'])->middleware(['role_or_permission:business.advisor.index,sanctum']);
        API::get('/{id}', [AdvisorController::class, 'show'])->middleware(['role_or_permission:business.advisor.show,sanctum'])->whereNumber('id');
        API::post('/', [AdvisorController::class, 'create'])->middleware(['role_or_permission:business.advisor.create,sanctum']);
        API::put('/{id}', [AdvisorController::class, 'update'])->middleware(['role_or_permission:business.advisor.update,sanctum'])->whereNumber('id');
        API::delete('/{id}', [AdvisorController::class, 'delete'])->middleware(['role_or_permission:business.advisor.delete,sanctum'])->whereNumber('id');
    });

    // donate
    API::group(['prefix' => 'donate'], function ($api) {

        API::get('/', [DonateController::class, 'index']);
        API::get('/{id}', [DonateController::class, 'show']);
        API::post('/', [DonateController::class, 'create']);
        API::delete('/{id}', [DonateController::class, 'delete']);
    });
    // transaction
    API::group(['prefix' => 'transactions'], function ($api) {

        API::get('/', [TransactionController::class, 'index']);
        API::get('/payment', [TransactionController::class, 'payment']);
        API::post('/verify', [TransactionController::class, 'verify']);
    });
    // sessions routes
    API::group(['prefix' => 'sessions', 'middleware' => ['api.auth']], function ($api) {
        API::get('/', [SessionController::class, 'index']);
        API::delete('/{id}', [SessionController::class, 'delete'])->whereNumber('id');
    });
});
