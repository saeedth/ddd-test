<?php

namespace App\Http\Requests\Api\Separator\Unit;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'english_name' => 'required|string|min:4|max:64',
            'persian_name' => 'required|string|min:4|max:64',
            'active' => 'boolean'
        ];
    }
}
