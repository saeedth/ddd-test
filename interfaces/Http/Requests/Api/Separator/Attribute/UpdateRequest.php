<?php

namespace App\Http\Requests\Api\Separator\Attribute;

use App\Domain\Separator\Enums\AttributeTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|string|min:4|max:255',
            'category_id' => 'sometimes|required|integer|exists:categories,id',
            'sort' => 'required|integer|min:1',
            'type' => [
                'required',
                Rule::in((AttributeTypeEnum::getValues()))
            ],
            'range' => 'required|integer|min:0',
            'unit_id' =>
                $this->input('type') !== AttributeTypeEnum::SWITCH
                    ? 'required|integer|exists:units,id' : 'sometimes|required|integer|exists:units,id',
            'active' => 'required|boolean',
            'filter' => 'required|boolean',
        ];
    }
}
