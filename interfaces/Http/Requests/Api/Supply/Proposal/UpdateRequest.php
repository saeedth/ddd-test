<?php

namespace App\Http\Requests\Api\Supply\Proposal;

use App\Domain\Account\Enums\ProposalStatusEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'category_id' => "required|integer|exists:categories,id",
            "area_id" => "required|integer|exists:areas,id",
            'requests' => "required|array",
            'requests.*' => "required|integer|exists:requests,id",
            "attributes" => "required|array",
            'status' => [
                "required_if",
                Rule::in(ProposalStatusEnum::getValues()),
            ]
        ];
    }
}
