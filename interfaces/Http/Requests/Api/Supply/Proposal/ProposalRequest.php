<?php

namespace App\Http\Requests\Api\Supply\Proposal;


use Illuminate\Foundation\Http\FormRequest;

class ProposalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // check category_id and attribute_id is match

        return [
            'mobile' => 'required|mobile',
            'proposal_id' => 'required|integer|exists:proposals,id'
        ];
    }
}
