<?php

namespace App\Http\Requests\Api\Supply\Proposal;


use App\Domain\Separator\Enums\AttributeTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // check category_id and attribute_id is match

        return [

            'title' => 'required|max:255',
            'sending_automatic_way' => 'required|boolean',
            'description' => 'required|max:255',
            'category_id' => 'required|integer|exists:categories,id',
            'area_id' => 'required|integer|exists:areas,id',
            'requests' => 'sometimes|required|array',
            'requests.*' => 'sometimes|required|integer',
            'attributes' => 'required|array',
            'attributes.*.id' => 'required|integer|exists:attributes,id',
            'attributes.*.type' => [
                'required',
                'integer',
                Rule::in(AttributeTypeEnum::getValues())
            ],
            'attributes.*.range' => 'required|integer'
            // 'status'=>[
            //     "required",
            //     Rule::in(ProposalStatusEnum::getValues()),
            // ]
        ];
    }
}
