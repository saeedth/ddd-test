<?php

namespace App\Http\Requests\Api\Demand\Request;

use App\Rules\AttributeValidator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'category_id' => 'required|integer|exists:categories,id',
            'description' => 'required|string',
            'areas' => 'required|array',
            'areas.*' => 'required|integer|exists:areas,id',
            'attributes' => [
                'required',
                'array',
                new AttributeValidator($this->category_id)
            ],
        ];
    }
}
