<?php

namespace App\Http\Requests\Api\Location\Province;


use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:4|max:255',
            'tell_prefix' => [
                'required',
                'digits:3',
            ],
            'active' => 'boolean'
        ];
    }
}
