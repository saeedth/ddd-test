<?php

namespace App\Http\Requests\Api\Location\Area;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:4|max:255',
            'aliases' => 'required|array',
            'aliases.*' => 'required|string|min:4|max:255',
            'city_id' => 'required|integer|exists:cities,id',
        ];
    }
}
