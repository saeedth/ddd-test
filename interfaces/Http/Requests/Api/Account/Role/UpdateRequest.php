<?php

namespace App\Http\Requests\Api\Account\Role;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "name" => [
                "required",
                "max:255",
                Rule::unique('roles', "name")->ignore($this->id),
            ],
            "guard_name" => [
                "sometimes",
                "max:255",
                Rule::unique('roles', "guard_name")->ignore($this->id),
            ],
            "permissions" => "sometimes|required|array",
            "permissions.*" => "required|integer|exists:permissions,id"
        ];
    }
}
