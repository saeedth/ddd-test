<?php

namespace App\Http\Requests\Api\Account\Role;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd(auth('api'));
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|min:4|max:255",
            "permissions" => "sometimes|required|array",
            "permissions.*" => "required|integer|exists:permissions,id"
        ];

    }
}
