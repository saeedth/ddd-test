<?php

namespace App\Http\Requests\Api\Account\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => [
                'required', "string", "mobile",
                Rule::unique('users')
            ],
            "city_id" => "exists:cities,id|nullable",
            "areas" => "sometimes|array",
            "areas.*" => "required|integer|exists:areas,id",
            "roles" => "required|array",
            "roles.*" => "required|integer|exists:roles,id"
        ];
    }
}
