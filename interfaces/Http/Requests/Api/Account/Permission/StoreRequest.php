<?php

namespace App\Http\Requests\Api\Account\Permission;

use Illuminate\Foundation\Http\FormRequest;


class StoreRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|max:255|unique:permissions,name",
            "guard_name" => "required|max:255|unique:permissions,guard_name",
        ];
    }
}
