<?php

namespace App\Http\Requests\Api\Auth\Login;

use Illuminate\Foundation\Http\FormRequest;

class VerifyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'mobile' => 'required|string|mobile',
            'code' => 'required|string|min:5|max:5'
        ];
    }
}
