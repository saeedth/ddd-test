<?php

namespace App\Http\Requests\Api\Auth\CheckExists;

use Illuminate\Foundation\Http\FormRequest;

class HandleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'mobile' => 'required|string|mobile'
        ];
    }
}
