<?php

namespace App\Http\Requests\Api\Business\Advisor;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_id" => "required|exists:categories,id",
            "area_id" => "required|exists:areas,id",
            "description" => "required|max:255",
            "phone" => "required",
            "name" => "required|max:255",
            "address" => "required|max:255",

            // "attributes"=>[
            //     'required',
            //     "array",
            //     new AttributeValidator($this->category_id)
            // ],
        ];
    }
}
