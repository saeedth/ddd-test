<?php

namespace App\Http\Requests\Api\Business\Donate;

use App\Domain\Demand\Entities\Request;
use App\Domain\Supply\Entities\Proposal;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "price" => "required",
            "user_id" => "nullable|exists:users,id",
            "description" => "required|max:255",
            "type" => ['nullable', Rule::in(['proposal', 'request'])],
            "id" => [
                "nullable",
                $this->type == "proposal" ?
                    Rule::in([Proposal::query()->pluck('id')])
                    :
                    Rule::in([Request::query()->pluck('id')])
            ],
        ];
    }
}
