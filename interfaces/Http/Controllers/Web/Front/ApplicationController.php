<?php

namespace App\Http\Controllers\Web\Front;

use App\Infrastructure\Controllers\Controller;

class ApplicationController extends Controller
{
    public function index()
    {
        return view('application');
    }
}
