<?php

namespace App\Http\Controllers\Api\Auth;

use Dingo\Api\Routing\Helpers;

class LogoutController
{
    use Helpers;

    public function logout()
    {
        $user = auth('sanctum')->user();
        $user->currentAccessToken()->delete();
        return $this->response->array([
            'status' => 'SUCCESS'
        ]);
    }

}
