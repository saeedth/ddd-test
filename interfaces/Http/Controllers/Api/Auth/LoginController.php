<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domain\Account\Contracts\UserRepository;
use App\Domain\Account\Enums\UserTypeEnum;
use App\Domain\Account\Events\UserLoginEvent;
use App\Domain\Account\Events\WelcomeToUser;
use App\Domain\Account\Transformers\UserLoginTransformer;
use App\Http\Requests\Api\Auth\Login\HandleRequest;
use App\Http\Requests\Api\Auth\Login\ResendRequest;
use App\Http\Requests\Api\Auth\Login\VerifyRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    use Helpers;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(HandleRequest $request)
    {

        $mobile = $request->input('mobile');
        $cacheKey = $this->cacheKey($mobile);

        if (Cache::has($cacheKey)) {
            $data = Cache::get($cacheKey);

            if ($this->checkTries($data['try'])) {
                return $this->response->array([
                    'status' => 'TO_MANY_REQUESTS',
                ]);
            }

            if ($this->checkLastTry($data['last_try'])) {
                return $this->response->array([
                    'status' => 'RECENTLY_SEND_REQUESTS',
                ]);
            }
        }

        $try = $data['try'] ?? 0;

        $data = $this->otpCacheData($try + 1);

        $status = Cache::put($this->cacheKey($mobile), $data, now()->addMinutes(15));

        $user = $this->userRepository->firstOrCreate($request->validated(), $request->validated() + ['type' => UserTypeEnum::NORMAL]);

        event(new UserLoginEvent($user, $data['code']));

        return $this->response->array([
            'status' => $status ? 'SUCCESS' : 'FAILED',
        ]);
    }

    public function resend(ResendRequest $request)
    {
        $mobile = $request->input('mobile');
        $cacheKey = $this->cacheKey($mobile);

        if (Cache::missing($cacheKey)) {
            return $this->response->array([
                'status' => 'MOBILE_NOT_REGISTERED',
            ]);
        }

        $data = Cache::get($cacheKey);

        if ($this->checkTries($data['try'])) {
            return $this->response->array([
                'status' => 'TO_MANY_REQUESTS',
            ]);
        }

        if ($this->checkLastTry($data['last_try'])) {
            return $this->response->array([
                'status' => 'RECENTLY_SEND_REQUESTS',
            ]);
        }

        $data = $this->otpCacheData($data['try'] + 1);

        $status = Cache::put($cacheKey, $data);

        $user = $this->userRepository->findFirstWhere('mobile', $request->input('mobile'));

        event(new UserLoginEvent($user, $data['code']));

        return $this->response->array([
            'status' => $status ? 'SUCCESS' : 'FAILED',
        ]);
    }

    public function verify(VerifyRequest $request)
    {

        $mobile = $request->input('mobile');
        $cacheKey = $this->cacheKey($mobile);


        if (Cache::missing($cacheKey)) {
            return $this->response->array([
                'status' => 'MOBILE_NOT_REGISTERED',
            ]);
        }

        $data = Cache::get($cacheKey);

        if (strcmp($data['code'], $request->input('code')) !== 0) {
            return $this->response->array([
                'status' => 'WRONG_CODE',
            ]);
        }
        $userData = [
            'mobile' => $mobile
        ];

        $user = $this->userRepository->firstOrCreate(Arr::only($userData, ['mobile']), $userData);

        $token = $user->createToken($request->userAgent())->plainTextToken;

        Cache::forget($cacheKey);

        // send sms to user
        event(new WelcomeToUser($user));
        // end send sms

        return $this->response->array([
            'status' => 'SUCCESS',
            'token' => $token,
            'user' => fractal()->item($user)->transformWith(new UserLoginTransformer($this->userRepository))->toArray(),
            'expiration' => config('sanctum.expiration') * 60,
            'token_type' => 'Bearer'
        ]);

    }

    private function otpCacheData($try = 1, $lastTry = null)
    {
        return [
            'code' => $this->generateOTPCode(),
            'try' => $try,
            'last_try' => $lastTry ?: now()->getTimestamp(),
        ];
    }

    private function generateOTPCode()
    {
        return rand(0, 9) . rand(10, 99) . rand(10, 99);
    }

    private function cacheKey($identifier)
    {
        return "user.login.verify:${identifier}";
    }

    private function checkLastTry($lastTry, $interval = 30)
    {
        return $lastTry + 30 >= now()->getTimestamp();
    }

    private function checkTries($try)
    {
        return $try > 5;
    }
}
