<?php

namespace App\Http\Controllers\Api\Auth;

use Dingo\Api\Routing\Helpers;

class RefreshTokenController
{
    use Helpers;

    public function refreshToken()
    {


        $user = auth('sanctum')->user();
        $user->currentAccessToken()->delete();
        $token = $user->createToken(request()->userAgent())->plainTextToken;

        return $this->response->array([
            'status' => 200,
            'token' => $token,
        ]);
    }

}
