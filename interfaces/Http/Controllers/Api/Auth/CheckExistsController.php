<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domain\Account\Contracts\UserRepository;
use App\Http\Requests\Api\Auth\CheckExists\HandleRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class CheckExistsController extends Controller
{
    use Helpers;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(HandleRequest $request)
    {
        $user = $this->userRepository->findWhere('mobile', $request->input('mobile'));

        return $this->response->array([
            'exists' => !blank($user)
        ]);
    }
}
