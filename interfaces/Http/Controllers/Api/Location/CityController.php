<?php

namespace App\Http\Controllers\Api\Location;

use App\Domain\Location\Contracts\CityRepository;
use App\Domain\Location\Transformers\CityTransformer;
use App\Http\Requests\Api\Location\City\StoreRequest;
use App\Http\Requests\Api\Location\City\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class CityController extends Controller
{

    use Helpers;

    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    /**
     * Show city
     *
     * Show city with id.
     *
     * @Get("cities/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"city.name" , "active":"city.active" ,"province":" city.province"})
     */
    public function show(int $id)
    {
        $city = $this->cityRepository->find($id);

        return $this->response()->item($city, new CityTransformer);
    }

    /**
     * Show list of cities
     *
     *
     * @Get("cities/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"city.name" , "active":"city.active" ,"province":" city.province"},....)
     *
     */
    public function index()
    {
        $queryBuilder = $this->cityRepository->filter();

        $cities = $queryBuilder->allowedFilters([
            'name',
            'active',
            AllowedFilter::exact('province_id')
        ])
            ->allowedSorts(['name'])
            ->paginate(10);
        return $this->response->paginator($cities, new CityTransformer());
    }


    /**
     * create a city
     *
     *
     * @post("cities/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $cityData = $request->validated();

        $this->cityRepository->create($cityData);

        return $this->response->created();
    }

    /**
     * update a city
     *
     * update city with id
     *
     * @put("cities/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $cityData = $request->validated();

        $this->cityRepository->update($request->id, $cityData);

        return $this->response->noContent();
    }

    /**
     * delete a city
     *
     * delete city with id
     *
     * @delete("cities/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $cityId)
    {
        $this->cityRepository->delete($cityId);

        return $this->response()->noContent();
    }
}
