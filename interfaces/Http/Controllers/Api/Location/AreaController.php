<?php

namespace App\Http\Controllers\Api\Location;

use App\Domain\Location\Contracts\AreaRepository;
use App\Domain\Location\Transformers\AreaTransformer;
use App\Http\Requests\Api\Location\Area\StoreRequest;
use App\Http\Requests\Api\Location\Area\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class AreaController extends Controller
{

    use Helpers;

    private $areaRepository;

    public function __construct(AreaRepository $areaRepository)
    {
        $this->areaRepository = $areaRepository;
    }

    /**
     * Show area
     *
     * Show area with id.
     *
     * @Get("areas/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"area.name" , "aliases":"area.aliases" ,"city_id":" area.city_id"})
     */
    public function show(int $id)
    {
        $area = $this->areaRepository->find($id);

        return $this->response()->item($area, new AreaTransformer);
    }

    /**
     * Show list of areas
     *
     *
     * @Get("areas/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"area.name" , "aliases":"area.aliases" ,"city_id":" area.city_id"},....])
     *
     */


    public function index()
    {
        $queryBuilder = $this->areaRepository->filter();

        $areas = $queryBuilder->allowedFilters([
            'name',
            AllowedFilter::exact('city_id')
        ])
            ->allowedSorts(['name', 'city_id'])
            ->paginate(10);

        return $this->response->paginator($areas, new AreaTransformer);
    }

    /**
     * create a area
     *
     *
     * @post("areas/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $areaData = $request->validated();

        $this->areaRepository->create($areaData);

        return $this->response->created();
    }

    /**
     * update a area
     *
     * update area with id
     *
     * @put("areas/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $areaData = $request->validated();

        $this->areaRepository->update($request->id, $areaData);

        return $this->response->noContent();
    }

    /**
     * delete a area
     *
     * delete area with id
     *
     * @delete("areas/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $areaId)
    {
        $this->areaRepository->delete($areaId);

        return $this->response()->noContent();
    }
}
