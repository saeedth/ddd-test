<?php

namespace App\Http\Controllers\Api\Location;

use App\Domain\Location\Contracts\ProvinceRepository;
use App\Domain\Location\Transformers\ProvinceTransformer;
use App\Http\Requests\Api\Location\Province\StoreRequest;
use App\Http\Requests\Api\Location\Province\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class ProvinceController extends Controller
{

    use Helpers;

    private $provinceRepository;

    public function __construct(ProvinceRepository $provinceRepository)
    {
        $this->provinceRepository = $provinceRepository;
    }

    /**
     * Show province
     *
     * Show province with id.
     *
     * @Get("provinces/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"province.first-name" , "last-name":"province.last-name" ,"mobile":" province.mobile","avatar": "province.avatar"})
     */
    public function show(int $id)
    {
        $province = $this->provinceRepository->find($id);

        return $this->response()->item($province, new ProvinceTransformer);
    }

    /**
     * Show list of provinces
     *
     *
     * @Get("provinces/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"province.name" , "tell_prefix":"province.tell_prefix" ,"active":"province.active"},
     * {"name":"province.name" , "tell_prefix":"province.tell_prefix" ,"active":"province.active"}]
     * )
     */
    public function index()
    {
        $queryBuilder = $this->provinceRepository->filter();
        $provinces = $queryBuilder->allowedFilters([
            'name',
            'tell_prefix',
            AllowedFilter::exact('active')
        ])
            ->allowedSorts(['name'])
            ->paginate(10);

        return $this->response->paginator($provinces, new ProvinceTransformer());
    }

    /**
     * create a province
     *
     *
     * @post("provinces/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $provinceData = $request->validated();

        $this->provinceRepository->create($provinceData);

        return $this->response->created();
    }

    /**
     * update a province
     *
     * update province with id
     *
     * @put("provinces/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $provinceData = $request->validated();

        $this->provinceRepository->update($request->id, $provinceData);

        return $this->response->noContent();
    }

    /**
     * delete a province
     *
     * delete province with id
     *
     * @delete("provinces/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $provinceId)
    {
        $this->provinceRepository->delete($provinceId);

        return $this->response()->noContent();
    }
}
