<?php

namespace App\Http\Controllers\Api\Supply;

use App\Domain\Account\Contracts\UserRepository;
use App\Domain\Demand\Contracts\RequestRepository;
use App\Domain\Demand\Events\SendProposalLikeRequestEvent;
use App\Domain\Demand\Events\SendProposalToSpecialUserEvent;
use App\Domain\Supply\Contracts\ProposalRepository;
use App\Domain\Supply\Transformers\ProposalTransformer;
use App\Http\Requests\Api\Supply\Proposal\ProposalRequest;
use App\Http\Requests\Api\Supply\Proposal\StoreRequest;
use App\Http\Requests\Api\Supply\Proposal\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Arr;
use Spatie\QueryBuilder\AllowedFilter;

class ProposalController extends Controller
{

    use Helpers;

    private $proposalRepository;
    private $requestRepository;
    private $userRepository;

    public function __construct(ProposalRepository $proposalRepository)
    {
        $this->proposalRepository = $proposalRepository;
//        $this->user=
    }

    /**
     * Show user
     *
     * Show user with id.
     *
     * @Get("users/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"})
     */
    public function show(int $id)
    {

        $proposal = $this->proposalRepository->find($id);

        return $this->response()->item($proposal, new ProposalTransformer);
    }

    /**
     * Show list of users
     *
     *
     * @Get("users/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"},
     * {"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"}]
     * )
     */

    public function index()
    {
        $queryBuilder = $this->proposalRepository->filter();
        $proposals = $queryBuilder->allowedFilters([
            'title',
            AllowedFilter::exact('user_id'),
            AllowedFilter::scope('category_id', 'category'),
            AllowedFilter::exact('area_id'),
            AllowedFilter::scope('areas'),
            AllowedFilter::scope('sending_automatic_way', 'sendingAutomaticWay'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('verify'),
            AllowedFilter::scope('attribute_id', 'attributeId'),
            AllowedFilter::scope('moreThan'),
            AllowedFilter::scope('lessThan'),
            AllowedFilter::scope('between'),
            AllowedFilter::scope('request_id', 'request'),

        ])
            ->allowedSorts(['title'])
            ->paginate(10);

        return $this->response->paginator($proposals, new ProposalTransformer);
    }

    /**
     * create a user
     *
     *
     * @post("users/")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function create(StoreRequest $request)
    {
        $proposalData = $request->validated();

        $proposalData['user_id'] = auth('sanctum')->id();

        // if proposal automatic send is true

        if ($proposalData['sending_automatic_way']) {

            $this->requestRepository = resolve(RequestRepository::class);

            $requests = $this->requestRepository->entity()::query()->category($proposalData['category_id'])->area($proposalData['area_id'])->get();

            $requestIds = $requests->pluck('id')->toArray();

            $differenceIds = array_key_exists('requests', $proposalData) ? array_diff($requestIds, $proposalData['requests']) : $requestIds;

            //if user don't choose all requests

            if ($differenceIds) {
                $query = null;
                foreach ($proposalData['attributes'] as $attribute) {
                    if ($attribute['type'] == 1) {
                        $query = $this->requestRepository->entity()::query()->whereIn('id', $differenceIds)->number($attribute);
                    } elseif ($attribute['type'] == 2) {
                        $query = $this->requestRepository->entity()::query()->whereIn('id', $differenceIds)->period($attribute);
                    }
                    $proposalData["requests"] = array_merge($proposalData['requests'] ?? [], $query->pluck('id')->toArray());
                }
                $proposalData["requests"] = array_unique($proposalData["requests"]);
            }
            $this->proposalRepository->create($proposalData);

            if ($proposalData["requests"]) {
                // send notification to users
                foreach ($requests as $request) {
                    if (in_array($request->id, $proposalData["requests"])) {
                        event(new SendProposalLikeRequestEvent($request->user, 1));
                    }
                }
            }

        } else {
            $this->proposalRepository->create($proposalData);
        }

        return $this->response->created();
    }

    /**
     * update a user
     *
     * update user with id
     *
     * @put("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $updateData = $request->validated();

        $this->proposalRepository->update($request->id, $updateData);

        return $this->response->noContent();
    }

    /**
     * delete a user
     *
     * delete user with id
     *
     * @delete("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $proposalId)
    {
        $this->proposalRepository->delete($proposalId);

        return $this->response()->noContent();
    }

    public function addProposalImageToGallery()
    {

        $proposal = $this->proposalRepository->find(request()->input('proposal_id'));

        $proposal->addMediaFromRequest('proposal')->toMediaCollection('proposal');

        return $this->response->created();
    }


    public function sendProposalToSpecialUser(ProposalRequest $request)
    {

        $this->userRepository = resolve(UserRepository::class);

        $userData = [
            "first_name" => null,
            "last_name" => null,
            "mobile" => $request->input('mobile'),
            "city_id" => null,
        ];

        $user = $this->userRepository->firstOrCreate(Arr::only($userData, ['mobile']), $userData);

        $proposal = $this->proposalRepository->find($request->input('proposal_id'));

        // send notification

        $attributes = $proposal->attributes()->where('special', true)->get();
        $newAttributes = [];

        foreach ($attributes as $attribute) {

            $newAttributes[] = ['name' => $attribute->name, 'value' => $attribute->pivot->value];
        }

        event(new SendProposalToSpecialUserEvent($user, $proposal->id, $newAttributes));
    }
}
