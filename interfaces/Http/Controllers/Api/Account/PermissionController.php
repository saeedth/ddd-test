<?php

namespace App\Http\Controllers\Api\Account;


use App\Domain\Account\Contracts\PermissionRepository;
use App\Domain\Account\Serializer\MySerializer;
use App\Domain\Account\Transformers\PermissionTransformer;
use App\Http\Requests\Api\Account\Permission\StoreRequest;
use App\Http\Requests\Api\Account\permission\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class PermissionController extends Controller
{
    use Helpers;

    private $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Show user
     *
     * Show user with id.
     *
     * @Get("users/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"})
     */
    public function show(int $id)
    {
        $permission = $this->permissionRepository->find($id);

        return $this->response()->item(
            $permission,
            new PermissionTransformer,
            [],
            fn($resource, $fractal) => $fractal->setSerializer(new MySerializer())
        );
    }

    /**
     * Show list of users
     *
     *
     * @Get("users/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"},
     * {"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"}]
     * )
     */


    public function index()
    {
        $permission = $this->permissionRepository->all();

        return $this->response
            ->collection($permission, new PermissionTransformer, [], fn($resource, $fractal) => $fractal->setSerializer(new MySerializer()));
    }

    /**
     * create a user
     *
     *
     * @post("users/")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function create(StoreRequest $request)
    {
        $permissionData = $request->validated();

        $this->permissionRepository->create($permissionData);

        return $this->response->created();
    }

    /**
     * update a user
     *
     * update user with id
     *
     * @put("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $permissionData = $request->validated();

        $this->permissionRepository->update($request->id, $permissionData);

        return $this->response->noContent();
    }

    /**
     * delete a user
     *
     * delete user with id
     *
     * @delete("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $permissionId)
    {
        $this->permissionRepository->delete($permissionId);

        return $this->response()->noContent();
    }
}
