<?php

namespace App\Http\Controllers\Api\Account;

use App\Domain\Account\Contracts\RoleRepository;
use App\Domain\Account\Transformers\RoleTransformer;
use App\Http\Requests\Api\Account\Role\StoreRequest;
use App\Http\Requests\Api\Account\Role\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class RoleController extends Controller
{

    use Helpers;

    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Show user
     *
     * Show user with id.
     *
     * @Get("users/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"})
     */
    public function show(int $id)
    {
        $role = $this->roleRepository->find($id);

        return $this->response()->item($role, new RoleTransformer);
    }

    /**
     * Show list of users
     *
     *
     * @Get("users/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"},
     * {"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"}]
     * )
     */


    public function index()
    {
        $querybuilder = $this->roleRepository->filter();

        $roles = $querybuilder->allowedFilters(['name'])->paginate(10);

        return $this->response->paginator($roles, new RoleTransformer);
    }

    /**
     * create a user
     *
     *
     * @post("users/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $roleData = $request->validated();

        $this->roleRepository->create($roleData);

        return $this->response->created();
    }

    /**
     * update a user
     *
     * update user with id
     *
     * @put("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $roleData = $request->validated();

        $role = $this->roleRepository->find($request->id);

        if ($role->editable) {

            $this->roleRepository->update($request->id, $roleData);

            return $this->response->noContent();
        }
        return $this->response->array([
            'status' => 'FAILED',
            'message' => 'امکان تغییر این نقش کاربری وجود ندارد'
        ]);

    }

    /**
     * delete a user
     *
     * delete user with id
     *
     * @delete("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $roleId)
    {
        $role = $this->roleRepository->find($roleId);

        if ($role->editable) {
            $this->roleRepository->delete($roleId);
            return $this->response()->noContent();
        }

        return $this->response->array([
            'status' => 'FAILED',
            'message' => 'امکان تغییر این نقش کاربری وجود ندارد'
        ]);

    }
}
