<?php

namespace App\Http\Controllers\Api\Account;

use App\Domain\Account\Contracts\UserRepository;
use App\Domain\Account\Transformers\UserTransformer;
use App\Http\Requests\Api\Account\User\StoreRequest;
use App\Http\Requests\Api\Account\User\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class UserController extends Controller
{

    use Helpers;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

    }

    /**
     * Show user
     *
     * Show user with id.
     *
     * @Get("users/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"})
     */
    public function show(int $id)
    {

        $user = $this->userRepository->find($id);

        // return $this->response()->item($user, new UserTransformer());
        return $this->response()->item($user, new UserTransformer());
    }

    /**
     * Show list of users
     *
     *
     * @Get("users/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"},
     * {"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"}]
     * )
     */


    public function index()
    {
        $queryBuilder = $this->userRepository->filter();

        $users = $queryBuilder->allowedFilters([
            'last_name',
            'first_name',
            AllowedFilter::exact('city_id'),
            AllowedFilter::scope('area_id', 'areaId'),
            AllowedFilter::scope('role_id', 'roleId'),
        ])
            ->allowedSorts(['first_name', 'last_name'])
            ->paginate(10);

        return $this->response->paginator($users, new UserTransformer());
    }

    /**
     * create a user
     *
     *
     * @post("users/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $userData = $request->validated();

        $this->userRepository->create($userData);

        return $this->response->created();
    }

    /**
     * update a user
     *
     * update user with id
     *
     * @put("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $updateData = $request->validated();

        $this->userRepository->update($request->id, $updateData);

        return $this->response->noContent();
    }

    /**
     * delete a user
     *
     * delete user with id
     *
     * @delete("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $userId)
    {
        $this->userRepository->delete($userId);

        return $this->response()->noContent();
    }
}
