<?php

namespace App\Http\Controllers\Api\Account;

use App\Domain\Account\Contracts\UserRepository;
use App\Domain\Account\Transformers\UserTransformer;
use App\Http\Requests\Api\Account\Profile\AvatarRequest;
use App\Http\Requests\Api\Account\Profile\EditRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class ProfileController extends Controller
{

    use Helpers;

    public $user;
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->user = $this->userRepository->loginUser('sanctum');
    }

    /**
     * Show user
     *
     * Show login User.
     *
     * @Get("user/profile")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"})
     */
    public function show()
    {
        return $this->response->item($this->user, new UserTransformer);
    }

    /**
     * Edit user
     *
     * Edit data for current user.
     *
     * @Post("user/edit/profile")
     * @Versions({"v1"})
     *
     * @Response(204)
     */
    public function edit(EditRequest $request)
    {

        $updateData = $request->validated();

        $this->userRepository->editProfile($this->user->id, $updateData);

        return $this->response->noContent();
    }

    /**
     * add avatar
     *
     * Add new user avatar.
     *
     * @Post("user/add/avatar")
     * @Versions({"v1"})
     *
     * @Response(201)
     *
     */

    public function addAvatar(AvatarRequest $request)
    {
        $this->user->addMediaFromRequest('avatar')->toMediaCollection('avatar');
        return $this->response->created();

    }
}
