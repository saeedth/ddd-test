<?php

namespace App\Http\Controllers\Api\Account;

use App\Domain\Account\Contracts\UserRepository;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class SessionController extends Controller
{

    use Helpers;

    private $userRepository;


    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function index()
    {
        $user = $this->userRepository->loginUser('sanctum');

        return $this->response->Array([
            'tokens' => $user->tokens()->get()->pluck('token')
        ]);
    }


    public function delete($tokenId)
    {
        $user = $this->userRepository->loginUser('sanctum');

        $user->tokens()->where([
            ['id', $tokenId],
            ['id', '<>', $user->currentAccessToken()->id]
        ])->delete();

        return $this->response()->noContent();
    }
}
