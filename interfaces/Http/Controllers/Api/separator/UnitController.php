<?php

namespace App\Http\Controllers\Api\separator;

use App\Domain\Separator\Contracts\UnitRepository;
use App\Domain\Separator\Transformers\UnitTransformer;
use App\Http\Requests\Api\Separator\Unit\StoreRequest;
use App\Http\Requests\Api\Separator\Unit\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class UnitController extends Controller
{

    use Helpers;

    private $unitRepository;

    public function __construct(UnitRepository $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }

    /**
     * Show user
     *
     * Show user with id.
     *
     * @Get("users/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"})
     */
    public function show(int $id)
    {
        $unit = $this->unitRepository->find($id);

        return $this->response()->item($unit, new UnitTransformer);
    }

    /**
     * Show list of users
     *
     *
     * @Get("users/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"},
     * {"first-name":"user.first-name" , "last-name":"user.last-name" ,"mobile":" user.mobile","avatar": "user.avatar"}]
     * )
     */


    public function index()
    {
        $queryBuilder = $this->unitRepository->filter();
        $units = $queryBuilder->allowedFilters([
            'english_name',
            'persian_name',
            AllowedFilter::exact('active'),
        ])
            ->allowedSorts([
                'english_name',
                'persian_name',
            ])
            ->paginate(15);

        return $this->response->paginator($units, new UnitTransformer());
    }

    /**
     * create a user
     *
     *
     * @post("users/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $unitData = $request->validated();

        $this->unitRepository->create($unitData);

        return $this->response->created();
    }

    /**
     * update a user
     *
     * update user with id
     *
     * @put("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $unitData = $request->validated();

        $this->unitRepository->update($request->id, $unitData);

        return $this->response->noContent();
    }

    /**
     * delete a user
     *
     * delete user with id
     *
     * @delete("users/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $unitId)
    {
        $this->unitRepository->delete($unitId);

        return $this->response()->noContent();
    }
}
