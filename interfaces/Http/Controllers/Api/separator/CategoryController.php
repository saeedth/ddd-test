<?php

namespace App\Http\Controllers\Api\separator;

use App\Domain\Separator\Contracts\CategoryRepository;
use App\Domain\Separator\Transformers\CategoryTransformer;
use App\Http\Requests\Api\Separator\Category\StoreRequest;
use App\Http\Requests\Api\Separator\Category\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class CategoryController extends Controller
{

    use Helpers;

    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Show category
     *
     * Show category with id.
     *
     * @Get("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"category.name" , "description":"category.description"})
     */
    public function show(int $id)
    {

        $category = $this->categoryRepository->find($id);

        return $this->response()->item($category, new CategoryTransformer);
    }

    /**
     * Show list of categories
     *
     *
     * @Get("categories/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"category.name" , "description":"category.description"},{"name":"category.name" , "description":"category.description"}]
     * )
     */
    public function index()
    {

        $querybuilder = $this->categoryRepository->filter();

        $categories = $querybuilder->allowedFilters([
            'name',
            AllowedFilter::exact('active')
        ])
            ->allowedSorts(['name'])
            ->paginate(10);

        return $this->response->paginator($categories, new CategoryTransformer());
    }

    /**
     * create a category
     *
     *
     * @post("categories/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $categoryData = $request->validated();

        $this->categoryRepository->create($categoryData);

        return $this->response->created();
    }

    /**
     * update a category
     *
     * update user with id
     *
     * @put("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $categoryData = $request->validated();

        $this->categoryRepository->update($request->id, $categoryData);

        return $this->response->noContent();
    }

    /**
     * delete a category
     *
     * delete category with id
     *
     * @delete("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $categoryId)
    {
        $this->categoryRepository->delete($categoryId);

        return $this->response()->noContent();
    }
}
