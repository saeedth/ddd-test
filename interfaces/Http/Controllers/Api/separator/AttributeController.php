<?php

namespace App\Http\Controllers\Api\separator;

use App\Domain\Separator\Contracts\AttributeRepository;
use App\Domain\Separator\Enums\AttributeTypeEnum;
use App\Domain\Separator\Transformers\AttributeTransformer;
use App\Http\Requests\Api\Separator\Attribute\StoreRequest;
use App\Http\Requests\Api\Separator\Attribute\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class AttributeController extends Controller
{

    use Helpers;

    private $attributeRepository;

    public function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Show category
     *
     * Show category with id.
     *
     * @Get("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"category.name" , "description":"category.description"})
     */
    public function show(int $id)
    {

        $attribute = $this->attributeRepository->find($id);

        return $this->response()->item($attribute, new AttributeTransformer);
    }

    /**
     * Show list of categories
     *
     *
     * @Get("categories/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"category.name" , "description":"category.description"},{"name":"category.name" , "description":"category.description"}]
     * )
     */


    public function index()
    {
        $queryBuilder = $this->attributeRepository->filter();

        $attributes = $queryBuilder->allowedFilters([
            'name',
            AllowedFilter::exact('active'),
            AllowedFilter::exact('filter'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('unit_id'),
        ])
            ->allowedSorts(['name'])
            ->paginate(15);

        foreach (AttributeTypeEnum::asArray() as $key => $value) {
            $types [] = ['id' => $value, 'name' => AttributeTypeEnum::getDescription($key)];
        }

        return $this->response->paginator($attributes, new AttributeTransformer())
            ->addMeta('types', $types);
    }

    /**
     * create a category
     *
     *
     * @post("categories/")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function create(StoreRequest $request)
    {
        $attributeData = $request->validated();

        $this->attributeRepository->create($attributeData);

        return $this->response->created();
    }

    /**
     * update a category
     *
     * update user with id
     *
     * @put("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $attributeData = $request->validated();

        $this->attributeRepository->update($request->id, $attributeData);

        return $this->response->noContent();
    }

    /**
     * delete a category
     *
     * delete category with id
     *
     * @delete("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $attributeId)
    {
        $this->attributeRepository->delete($attributeId);

        return $this->response()->noContent();
    }
}
