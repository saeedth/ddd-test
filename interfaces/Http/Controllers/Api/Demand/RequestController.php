<?php

namespace App\Http\Controllers\Api\Demand;

use App\Domain\Demand\Contracts\RequestRepository;
use App\Domain\Demand\Events\SendProposalLikeRequestEvent;
use App\Domain\Demand\Events\SendRequestTrackingCodeToUserEvent;
use App\Domain\Demand\Transformers\RequestTransformer;
use App\Domain\Supply\Contracts\ProposalRepository;
use App\Http\Requests\Api\Demand\Request\AssignProposalsRequest;
use App\Http\Requests\Api\Demand\Request\StoreRequest;
use App\Http\Requests\Api\Demand\Request\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class RequestController extends Controller
{
    use Helpers;

    private $requestRepository;
    private $proposalRepository;
    private $user = null;


    public function __construct(RequestRepository $requestRepository)
    {
        $this->requestRepository = $requestRepository;
        $this->user = auth('sanctum')->user();
    }

    /**
     * Show category
     *
     * Show category with id.
     *
     * @Get("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"category.name" , "description":"category.description"})
     */
    public function show(int $id)
    {
        $request = $this->requestRepository->find($id);
        return $this->response()->item($request, new RequestTransformer);
    }

    /**
     * Show list of categories
     *
     *
     * @Get("categories/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"category.name" , "description":"category.description"},{"name":"category.name" , "description":"category.description"}]
     * )
     */


    public function index()
    {

        $queryBuilder = $this->requestRepository->filter();

        $requests = $queryBuilder->allowedFilters([
            AllowedFilter::scope('category_id', 'category'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact("status"),
            AllowedFilter::exact('tracking_code'),
            AllowedFilter::scope('mobile'),
            AllowedFilter::scope('area_id', "area"),
            AllowedFilter::scope('proposal_id', "proposal"),
        ])
            ->allowedSorts(['status'])
            ->paginate(10);

        return $this->response->paginator($requests, new RequestTransformer);
    }

    /**
     * create a category
     *
     *
     * @post("categories/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $requestData = $request->validated();

        $requestData['user_id'] = $this->user->id;

        $this->proposalRepository = resolve(ProposalRepository::class);

        $proposals = $this->proposalRepository->entity()::query()
            ->category($requestData['category_id'])
            ->areas($requestData['areas'])
            ->sendingAutomaticWay(true)
            ->get();

        $newProposalIds = [];
        foreach ($proposals as $proposal) {
            foreach ($proposal->attributes as $attribute) {
                if ($attribute->type == 1) {
                    foreach ($requestData['attributes']['number'] as $number) {
                        if ($number['id'] == $attribute->id &&
                            $attribute->pivot->value >= $number['value'] - $attribute->range &&
                            $attribute->pivot->value <= $number['value'] + $attribute->range) {
                            if (!in_array($proposal->id, $newProposalIds)) {
                                $newProposalIds[] = $proposal->id;
                            }
                        }
                    }
                }

                if ($attribute->type == 2) {
                    foreach ($requestData['attributes']['period'] as $period) {
                        if ($period['id'] == $attribute->id &&
                            $attribute->pivot->value >= $period['from'] - $attribute->range &&
                            $attribute->pivot->value <= $period['to'] + $attribute->range) {
                            if (!in_array($proposal->id, $newProposalIds)) {
                                $newProposalIds[] = $proposal->id;
                            }
                        }
                    }
                }
            }
        }
        //create random tracking code
        $trackingCode = $this->generateTrackingCode();
        $requestData['tracking_code'] = $trackingCode;
        $request = $this->requestRepository->create($requestData);
        // send request tracking code Url to user
        event(new SendRequestTrackingCodeToUserEvent($this->user, $trackingCode));

        if ($newProposalIds) {
            $this->requestRepository->saveProposalToRequest($request, $newProposalIds);
            // send a notification when find proposal related request
            event(new SendProposalLikeRequestEvent($this->user, $request->id, count($newProposalIds)));

        }
        return $this->response->created();
    }

    /**
     * update a category
     *
     * update user with id
     *
     * @put("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $requestData = $request->validated();

        $this->requestRepository->update($request->id, $requestData);

        return $this->response->noContent();
    }

    /**
     * delete a category
     *
     * delete category with id
     *
     * @delete("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $requestId)
    {
        $this->requestRepository->delete($requestId);

        return $this->response()->noContent();
    }

    public function sendProposalsToSpecialRequest(int $requestId, AssignProposalsRequest $assignRequest)
    {
        $proposals = $assignRequest->get('proposals');
        $request = $this->requestRepository->assignProposalsToSpecialRequest($requestId, $proposals);
        // send notification to user who create a rqs
        event(new SendProposalLikeRequestEvent($request->user, $request->id, count($proposals)));
        return $this->response->created();

    }

    public function generateTrackingCode()
    {

        return rand(rand(0, 9), rand(0, 9)) .
            rand(rand(0, 9), rand(0, 9)) .
            rand(rand(0, 9), rand(0, 9)) .
            rand(rand(0, 9), rand(0, 9)) .
            rand(rand(0, 9), rand(0, 9)) .
            rand(rand(0, 9), rand(0, 9));
    }
}
