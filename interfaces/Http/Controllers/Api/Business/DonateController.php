<?php

namespace App\Http\Controllers\Api\Business;

use App\Domain\Business\Contracts\DonateRepository;
use App\Domain\Business\Transformers\DonateTransformer;
use App\Http\Requests\Api\Business\Donate\StoreRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class DonateController extends Controller
{

    use Helpers;

    private $donateRepository;

    public function __construct(DonateRepository $donateRepository)
    {
        $this->donateRepository = $donateRepository;
    }

    /**
     * Show category
     *
     * Show category with id.
     *
     * @Get("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"category.name" , "description":"category.description"})
     */
    public function show(int $id)
    {
        $donate = $this->donateRepository->find($id);

        return $this->response()->item($donate, new DonateTransformer);
    }

    /**
     * Show list of categories
     *
     *
     * @Get("categories/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"category.name" , "description":"category.description"},{"name":"category.name" , "description":"category.description"}]
     * )
     */


    public function index()
    {


        $queryBuilder = $this->donateRepository->filter();

        $donates = $queryBuilder->allowedFilters([
            AllowedFilter::exact('user_id'),
        ])->paginate(10);

        return $this->response->paginator($donates, new DonateTransformer);
    }

    /**
     * create a category
     *
     *
     * @post("categories/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $requestData = $request->validated();

        $this->donateRepository->create($requestData);

        return $this->response->created();
    }


    /**
     * delete a category
     *
     * delete category with id
     *
     * @delete("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $donateId)
    {
        $this->donateRepository->delete($donateId);

        return $this->response()->noContent();
    }
}
