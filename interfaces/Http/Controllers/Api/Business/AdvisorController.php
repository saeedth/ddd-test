<?php

namespace App\Http\Controllers\Api\Business;

use App\Domain\Business\Contracts\AdvisorRepository;
use App\Domain\Business\Transformers\AdvisorTransformer;
use App\Http\Requests\Api\Business\Advisor\StoreRequest;
use App\Http\Requests\Api\Business\Advisor\UpdateRequest;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Spatie\QueryBuilder\AllowedFilter;

class AdvisorController extends Controller
{

    use Helpers;

    private $advisorRepository;

    public function __construct(AdvisorRepository $advisorRepository)
    {
        $this->advisorRepository = $advisorRepository;
    }

    /**
     * Show category
     *
     * Show category with id.
     *
     * @Get("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response(200, body={"name":"category.name" , "description":"category.description"})
     */
    public function show(int $id)
    {

        $advisor = $this->advisorRepository->find($id);

        return $this->response()->item($advisor, new AdvisorTransformer);
    }

    /**
     * Show list of categories
     *
     *
     * @Get("categories/")
     * @Versions({"v1"})
     *
     * @Response(200, body=[{"name":"category.name" , "description":"category.description"},{"name":"category.name" , "description":"category.description"}]
     * )
     */


    public function index()
    {
        $queryBuilder = $this->advisorRepository->filter();
        $advisors = $queryBuilder->allowedFilters([
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('area_id'),
        ])->paginate(10);

        return $this->response->paginator($advisors, new AdvisorTransformer);
    }

    /**
     * create a category
     *
     *
     * @post("categories/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function create(StoreRequest $request)
    {
        $requestData = $request->validated();

        $requestData['user_id'] = auth('sanctum')->id();

        $this->requestRepository->create($requestData);

        return $this->response->created();
    }

    /**
     * update a category
     *
     * update user with id
     *
     * @put("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function update(UpdateRequest $request)
    {
        $requestData = $request->validated();

        $this->requestRepository->update($request->id, $requestData);

        return $this->response->noContent();
    }

    /**
     * delete a category
     *
     * delete category with id
     *
     * @delete("categories/{id}")
     * @Versions({"v1"})
     *
     * @Response()
     */
    public function delete(int $requestId)
    {
        $this->requestRepository->delete($requestId);

        return $this->response()->noContent();
    }
}
