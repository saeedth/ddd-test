<?php

namespace App\Http\Controllers\Api\Business;

use App\Domain\Business\Contracts\TransactionRepository;
use App\Domain\Business\Transformers\DonateTransformer;
use App\Infrastructure\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;
use Spatie\QueryBuilder\AllowedFilter;

class TransactionController extends Controller
{

    use Helpers;

    private $transactionRepository;

    private $transactionId = null;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function index()
    {
        $queryBuilder = $this->transactionRepository->filter();

        $transactions = $queryBuilder->allowedFilters([
            AllowedFilter::exact('status'),
        ])->paginate(10);

        return $this->response->paginator($transactions, new DonateTransformer);
    }

    /**
     * create a category
     *
     *
     * @post("categories/")
     * @Versions({"v1"})
     *
     * @Response()
     */


    public function payment()
    {

        $invoice = new Invoice();
        $invoice->amount(5000);
        $invoice->detail([
            'company_id' => 20,
            'package_id' => 18,
            'discount' => 5,
        ]);

        return Payment::purchase(
            $invoice,
            function ($driver, $transactionId) {
                $this->transactionId = $transactionId;
            }
        )->pay()->render();
    }


    public function verify()
    {

        try {
            $receipt = Payment::amount(1000)->transactionId(request()->transaction_id)->verify();

            // You can show payment referenceId to the user.
            echo $receipt->getReferenceId();

            // add record to transaction table


            // return success response

            return $this->response->toArray([

                'status' => 'SUCCESS',
                'msg' => "SuccessFull"

            ]);
        } catch (InvalidPaymentException $exception) {

            return $this->response->toArray([

                'status' => 'FAILED',
                'msg' => $exception->getMessage()

            ]);
        }
    }
}
