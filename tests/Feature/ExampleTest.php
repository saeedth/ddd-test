<?php

namespace Tests\Feature;

use App\Domain\Account\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{

    /**
     * @test
     */

    public function can_user_login()
    {

        $response=$this->withHeaders([
            'Accept'=>"application/vnd.yareaval.v1+json"
        ])->post('api/auth/login',[
            'mobile'=>"09302474269",
        ]);

        dd($response->status());
        
    }
    /**
     * @tes
     */
  
    public function can_user_refresh_token()
    {
        
        // dd("ssss");
        $user=User::query()->first();
        $this->actingAs($user,'api')->withToken('123456',"Bearer");
        $response=$this->withHeaders([
            'Accept'=>"application/vnd.yareaval.v1+json"
        ])->get('api/auth/refreshToken');

        dd($response);
        
    }
}
