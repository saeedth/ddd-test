<?php

namespace App\Console\Commands;

use App\Infrastructure\Jobs\RemoveExpiresTokens;
use Illuminate\Console\Command;

class ExpiresToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When This Command is executed .. All Expires token in Personal-access-token is removed ... ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        RemoveExpiresTokens::dispatch();
    }
}
