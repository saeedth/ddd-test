<?php

namespace App\Providers;

use App\Domain\Account\Contracts\PermissionRepository as PermissionRepositoryInterface;
use App\Domain\Account\Contracts\PersonalAccessTokenRepository as PersonalAccessTokenRepositoryInterface;
use App\Domain\Account\Contracts\RoleRepository as RoleRepositoryInterface;
use App\Domain\Account\Contracts\UserRepository as UserRepositoryInterface;
use App\Domain\Account\Repositories\Eloquent\PermissionRepository;
use App\Domain\Account\Repositories\Eloquent\PersonalAccessTokenRepository;
use App\Domain\Account\Repositories\Eloquent\RoleRepository;
use App\Domain\Account\Repositories\Eloquent\UserRepository;
use App\Domain\Business\Contracts\AdvisorRepository as AdvisorRepositoryInterface;
use App\Domain\Business\Contracts\DonateRepository as DonateRepositoryInterface;
use App\Domain\Business\Contracts\TransactionRepository as TransactionRepositoryInterface;
use App\Domain\Business\Repositories\Eloquent\AdvisorRepository;
use App\Domain\Business\Repositories\Eloquent\DonateRepository;
use App\Domain\Business\Repositories\Eloquent\TransactionRepository;
use App\Domain\Demand\Contracts\RequestRepository as RequestRepositoryInterface;
use App\Domain\Demand\Repositories\Eloquent\RequestRepository;
use App\Domain\Location\Contracts\AreaRepository as AreaRepositoryInterface;
use App\Domain\Location\Contracts\CityRepository as CityRepositoryInterface;
use App\Domain\Location\Contracts\ProvinceRepository as ProvinceRepositoryInterface;
use App\Domain\Location\Repositories\Eloquent\AreaRepository;
use App\Domain\Location\Repositories\Eloquent\CityRepository;
use App\Domain\Location\Repositories\Eloquent\ProvinceRepository;
use App\Domain\Separator\Contracts\UnitRepository as UnitRepositoryInterface;
use App\Domain\Separator\Repositories\Eloquent\UnitRepository;
use App\Domain\Separator\Contracts\AttributeRepository as AttributeRepositoryInterface;
use App\Domain\Separator\Contracts\CategoryRepository as CategoryRepositoryInterface;
use App\Domain\Separator\Repositories\Eloquent\AttributeRepository;
use App\Domain\Separator\Repositories\Eloquent\CategoryRepository;
use App\Domain\Supply\Contracts\ProposalRepository as ProposalRepositoryInterface;
use App\Domain\Supply\Repositories\Eloquent\ProposalRepository;
use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

        $this->app->bind(ProvinceRepositoryInterface::class, ProvinceRepository::class);

        $this->app->bind(RequestRepositoryInterface::class, RequestRepository::class);

        $this->app->bind(AttributeRepositoryInterface::class, AttributeRepository::class);

        $this->app->bind(AreaRepositoryInterface::class, AreaRepository::class);

        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);

        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);

        $this->app->bind(UnitRepositoryInterface::class, UnitRepository::class);

        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);

        $this->app->bind(AdvisorRepositoryInterface::class, AdvisorRepository::class);

        $this->app->bind(ProposalRepositoryInterface::class, ProposalRepository::class);

        $this->app->bind(PermissionRepositoryInterface::class, PermissionRepository::class);

        $this->app->bind(PersonalAccessTokenRepositoryInterface::class, PersonalAccessTokenRepository::class);

        $this->app->bind(TransactionRepositoryInterface::class, TransactionRepository::class);

        $this->app->bind(DonateRepositoryInterface::class, DonateRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
