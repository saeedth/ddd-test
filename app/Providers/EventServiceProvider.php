<?php

namespace App\Providers;

use App\Domain\Account\Events\UserLoginEvent;
use App\Domain\Account\Events\WelcomeToUser;
use App\Domain\Account\Listeners\UserLoginListener;
use App\Domain\Account\Listeners\WelcomeToUserListener;
use App\Domain\Demand\Events\SendProposalLikeRequestEvent;
use App\Domain\Demand\Events\SendProposalToSpecialUserEvent;
use App\Domain\Demand\Events\SendRequestTrackingCodeToUserEvent;
use App\Domain\Demand\Listeners\SendProposalLikeRequestListener;
use App\Domain\Demand\Listeners\SendProposalToSpecialUserListener;
use App\Domain\Demand\Listeners\SendRequestTrackingCodeToUserListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserLoginEvent::class => [
            UserLoginListener::class,
        ],

        WelcomeToUser::class => [
            WelcomeToUserListener::class
        ],
        SendRequestTrackingCodeToUserEvent::class => [
            SendRequestTrackingCodeToUserListener::class
        ],

        SendProposalLikeRequestEvent::class => [
            SendProposalLikeRequestListener::class
        ],

        SendProposalToSpecialUserEvent::class => [
            SendProposalToSpecialUserListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
