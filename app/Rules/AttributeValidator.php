<?php

namespace App\Rules;

use App\Domain\Seprator\Entities\Attribute;
use Illuminate\Contracts\Validation\Rule;

class AttributeValidator implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */

    private $categoryId;

    public function __construct(int $categoryId)
    {

        $this->categoryId = $categoryId;
    }

    public $message = '';

    public function passes($attribute, $value)
    {
        foreach ($value as $key => $attribute) {
            if ($key == 'number') {
                foreach ($attribute as $value) {
                    if (!$this->checkAttributeExists($value['id'])) {
                        $this->message = "The attribute {$value['id']} is invalid";
                        return false;
                    }
                    if (!is_integer((int)$value['value'])) {
                        $this->message = "The value of attribute {$value['id']} must be integer";
                        return false;
                    }
                }
            }
            if ($key == 'period') {

                foreach ($attribute as $value) {
                    if (!$this->checkAttributeExists($value['id'])) {
                        $this->message = "The attribue {$value['id']} is invalid";
                        return false;
                    }
                    if (!is_integer((int)$value['from'])) {
                        $this->message = "The { from parameter } of attribue {$value['id']} must be integer";
                        return false;
                    }

                    if (!is_integer((int)$value['to'])) {
                        $this->message = "The { to parameter } of attribue {$value['id']} must be integer";
                        return false;
                    }

                    if ((int)$value['from'] >= (int)$value['to']) {
                        $this->message = "The { to parameter } of attribue {$value['id']} must be greater than { from parameter }";
                        return false;
                    }
                }
            }
            if ($key == 'switch') {

                foreach ($attribute as $value) {
                    if (!$this->checkAttributeExists($value['id'])) {
                        $this->message = "The attribue {$value['id']} is invalid";
                        return false;
                    }
                    if (!is_bool($value['value'])) {
                        $this->message = "The value of attribue {$value['id']} must be boolean";
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function checkAttributeExists($attributeId)
    {
        return Attribute::query()->where([['id', $attributeId], ['category_id', $this->categoryId]])->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
