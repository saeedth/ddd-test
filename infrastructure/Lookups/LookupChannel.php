<?php

namespace App\Infrastructure\Lookups;

use Illuminate\Notifications\Notification;
use Kavenegar\KavenegarApi;

abstract class LookupChannel
{
    protected $api;

    public const SMS = 'sms';
    public const CALL = 'call';

    private $receptor;

    private $type;

    protected $token;
    protected $token2;
    protected $token3;
    protected $token10;
    protected $token20;

    private $template;

    protected $notifiable;
    protected Notification $notification;

    /**
     * KavenegarChannel constructor.
     *
     */
    public function __construct()
    {
        $this->api = new KavenegarApi(config('services.kavenegar.key'));

        $this->type = $this->type();
        $this->template = $this->template();
    }

    abstract protected function template(): string;

    abstract protected function token($notifiable, Notification $notification): string;


    protected function receptor($notifiable, Notification $notification)
    {
        if (method_exists($notifiable, 'sendMessageRoute')) {
            return $notifiable->sendMessageRoute() ?: $notifiable->mobile;
        }

        return $notifiable->mobile;
    }

    protected function type(): string
    {
        return static::SMS;
    }

    protected function token2($notifiable, Notification $notification): string
    {
        return '';
    }

    protected function token3($notifiable, Notification $notification): string
    {
        return '';
    }

    protected function token10($notifiable, Notification $notification): string
    {
        return '';
    }

    protected function token20($notifiable, Notification $notification): string
    {
        return '';
    }

    public function send($notifiable, Notification $notification)
    {
        $this->receptor = $this->receptor($notifiable, $notification);

        $this->token = str_replace(' ', '_', $this->token($notifiable, $notification));
        $this->token2 = str_replace(' ', '_', $this->token2($notifiable, $notification));
        $this->token3 = str_replace(' ', '_', $this->token3($notifiable, $notification));
        $this->token10 = $this->token10($notifiable, $notification);
        $this->token20 = $this->token20($notifiable, $notification);

        return $this->verifyLookup();
    }

    protected function verifyLookup()
    {
        return $this->api->VerifyLookup(
            $this->receptor,
            $this->token,
            $this->token2,
            $this->token3,
            $this->template,
            $this->type,
            $this->token10,
            $this->token20
        );
    }
}
