<?php

namespace App\Infrastructure\Repositories;

abstract class SequenceAbstract
{
    protected $entity;

    abstract public function make();

    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;

    }
}
