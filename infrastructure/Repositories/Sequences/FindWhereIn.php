<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class FindWhereIn extends SequenceAbstract
{
    protected $value;
    protected $column;

    public function __construct($column, $value)
    {
        $this->column = $column;
        $this->value = $value;
    }

    public function make()
    {
        return $this->entity->whereIn($this->column, $this->value)->get();
    }
}
