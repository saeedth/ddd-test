<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Create extends SequenceAbstract
{
    protected $attributes;

    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;
    }

    public function make()
    {
        return $this->entity->create($this->attributes);
    }
}
