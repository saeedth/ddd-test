<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Cursor extends SequenceAbstract
{
    public function make()
    {
        return $this->entity->cursor();
    }
}
