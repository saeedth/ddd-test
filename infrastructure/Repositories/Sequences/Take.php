<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Take extends SequenceAbstract
{
    protected $count;

    public function __construct($count)
    {
        $this->count = $count;
    }

    public function make()
    {
        return $this->entity->limit($this->count)->get();
    }
}
