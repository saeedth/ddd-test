<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Count extends SequenceAbstract
{
    protected $columns;

    public function __construct($columns = '*')
    {
        $this->columns = $columns;
    }

    public function make()
    {
        return $this->entity->count();
    }
}
