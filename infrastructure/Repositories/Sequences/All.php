<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class All extends SequenceAbstract
{
    public function make()
    {
        return $this->entity->get();
    }
}
