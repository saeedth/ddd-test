<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;
use Spatie\QueryBuilder\QueryBuilder;

class Sort extends SequenceAbstract
{

    protected $sorts = [];

    public function __construct($sorts)
    {

        $this->sorts = $sorts;
    }

    public function make()
    {
        return QueryBuilder::for($this->entity)
            ->allowedSorts($this->sorts)->get();

    }
}
