<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class DD extends SequenceAbstract
{
    public function make()
    {
        $this->entity->dd();
    }
}
