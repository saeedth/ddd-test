<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class WhereDelete extends SequenceAbstract
{
    protected $column;
    protected $operator;
    protected $value;


    public function __construct($column, $operator = "=", $value)
    {
        $this->column = $column;
        $this->operator = $operator;
        $this->value = $value;

    }

    public function make()
    {
        return $this->entity->where($this->column, $this->operator, $this->value)->delete();
    }
}
