<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Delete extends SequenceAbstract
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function make()
    {
        return $this->entity->findOrFail($this->id)->delete();
    }
}
