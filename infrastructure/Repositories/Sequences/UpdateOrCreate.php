<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class UpdateOrCreate extends SequenceAbstract
{
    protected $conditions;
    protected $properties;

    public function __construct($conditions, $properties)
    {
        $this->conditions = $conditions;
        $this->properties = $properties;
    }

    public function make()
    {
        return $this->entity->updateOrCreate($this->conditions, $this->properties);
    }
}
