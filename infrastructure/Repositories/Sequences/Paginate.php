<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Paginate extends SequenceAbstract
{
    protected $perPage;

    public function __construct($perPage = 10)
    {
        $this->perPage = $perPage;
    }

    public function make()
    {
        return $this->entity->paginate($this->perPage);
    }
}
