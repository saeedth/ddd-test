<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class FirstOrCreate extends SequenceAbstract
{
    protected $properties;
    protected $values;

    public function __construct($properties, $values = [])
    {
        $this->properties = $properties;
        $this->values = $values;
    }

    public function make()
    {
        return $this->entity->firstOrCreate($this->properties, $this->values);
    }
}
