<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class First extends SequenceAbstract
{
    public function make()
    {
        return $this->entity->firstOrFail();
    }
}
