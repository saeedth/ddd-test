<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class Update extends SequenceAbstract
{
    protected $id;
    protected $properties;

    public function __construct($id, $properties)
    {
        $this->id = $id;
        $this->properties = $properties;
    }

    public function make()
    {
        return $this->entity->findOrFail($this->id)->update($this->properties);
    }
}
