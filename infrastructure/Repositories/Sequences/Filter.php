<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;
use Spatie\QueryBuilder\QueryBuilder;

class Filter extends SequenceAbstract
{

    public function make()
    {
        return QueryBuilder::for($this->entity);
    }
}
