<?php

namespace App\Infrastructure\Repositories\Sequences;

use App\Infrastructure\Repositories\SequenceAbstract;

class FindWhere extends SequenceAbstract
{
    protected $value;
    protected $operator;
    protected $column;

    public function __construct($column, $value, $operator = '=')
    {
        $this->column = $column;
        $this->value = $value;
        $this->operator = $operator;
    }

    public function make()
    {
        return $this->entity->where($this->column, $this->operator, $this->value)->get();
    }
}
