<?php

namespace App\Infrastructure\Repositories;

use App\Infrastructure\Repositories\Sequences\All;
use App\Infrastructure\Repositories\Sequences\Count;
use App\Infrastructure\Repositories\Sequences\Create;
use App\Infrastructure\Repositories\Sequences\Cursor;
use App\Infrastructure\Repositories\Sequences\DD;
use App\Infrastructure\Repositories\Sequences\Delete;
use App\Infrastructure\Repositories\Sequences\Filter;
use App\Infrastructure\Repositories\Sequences\Find;
use App\Infrastructure\Repositories\Sequences\FindFirstWhere;
use App\Infrastructure\Repositories\Sequences\FindWhere;
use App\Infrastructure\Repositories\Sequences\FindWhereIn;
use App\Infrastructure\Repositories\Sequences\First;
use App\Infrastructure\Repositories\Sequences\FirstOrCreate;
use App\Infrastructure\Repositories\Sequences\Paginate;
use App\Infrastructure\Repositories\Sequences\Sort;
use App\Infrastructure\Repositories\Sequences\Take;
use App\Infrastructure\Repositories\Sequences\Update;
use App\Infrastructure\Repositories\Sequences\UpdateOrCreate;
use App\Infrastructure\Repositories\Sequences\WhereDelete;
use Illuminate\Support\Arr;

abstract class EloquentRepository
{
    protected $entity;

    private $sequences = [
        'all' => All::class,
        'count' => Count::class,
        'create' => Create::class,
        'cursor' => Cursor::class,
        'dd' => DD::class,
        'delete' => Delete::class,
        'find' => Find::class,
        'findFirstWhere' => FindFirstWhere::class,
        'findWhere' => FindWhere::class,
        'findWhereIn' => FindWhereIn::class,
        'first' => First::class,
        'firstOrCreate' => FirstOrCreate::class,
        'paginate' => Paginate::class,
        'take' => Take::class,
        'update' => Update::class,
        'updateOrCreate' => UpdateOrCreate::class,
        'WhereDelete' => WhereDelete::class,
        'filter' => Filter::class,
        'sort' => Sort::class
    ];

    abstract public function entity();

    public function __construct()
    {
        $this->entity = $this->resolveEntity();
    }

    public function __call($name, $arguments)
    {
        return (new $this->sequences[$name](...$arguments))
            ->setEntity($this->entity)
            ->make();
    }

    public function withCriteria(...$criteria)
    {
        $criteria = Arr::flatten($criteria);

        foreach ($criteria as $criterion) {
            $this->entity = $criterion->apply($this->entity);
        }
        return $this;
    }

    protected function resolveEntity()
    {
        return app()->make($this->entity());
    }
}
