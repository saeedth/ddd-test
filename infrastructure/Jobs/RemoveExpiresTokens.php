<?php

namespace App\Infrastructure\Jobs;

use App\Domain\Account\Contracts\PersonalAccessTokenRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RemoveExpiresTokens implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $personalAccessTokenRepository;

    public function __construct()
    {
        $this->personalAccessTokenRepository = resolve(PersonalAccessTokenRepository::class);
    }

    public function handle()
    {
        return $this->personalAccessTokenRepository->WhereDelete('created_at', '<=', now()->subMinutes(config('sanctum.expiration')));
    }
}
