<?php

namespace App\Infrastructure\Dingo\AuthProviders;

use Dingo\Api\Contract\Auth\Provider;
use Dingo\Api\Routing\Route;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class SanctumAuthProvider  implements Provider
{
    /**
     * Illuminate authentication manager.
     *
     * @var AuthManager
     */
    private $auth;

    /**
     * Create a new basic provider instance.
     *
     * @param AuthManager $auth
     *
     * @return void
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Authenticate request with Basic.
     *
     * @param Request $request
     * @param Route $route
     *
     * @return mixed
     */
    public function authenticate(Request $request, Route $route)
    {
        if ($user = $this->auth->guard('sanctum')->user()) {
            return $user;
        }

        throw new UnauthorizedHttpException('',
            'Unauthenticated'
        );
    }
}
