<?php

namespace App\Infrastructure\Contracts\Request;


abstract class StoreStrategy
{

    protected $requestRepository;

    public abstract function store(array $requestData);
}
