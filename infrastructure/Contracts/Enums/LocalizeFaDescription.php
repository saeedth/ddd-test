<?php

namespace App\Infrastructure\Contracts\Enums;

interface LocalizeFaDescription
{
    public static function getLocalizeFaDescription(): array;
}
