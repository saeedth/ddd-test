<?php

namespace App\Domain\Business\Entities;

use App\Domain\Account\Entities\User;
use App\Domain\Location\Entities\Area;
use App\Domain\Seprator\Entities\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advisor extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'category_id',
        'user_id',
        'area_id',
        'name',
        'description',
        'address',
        'phone',
        'verify',
        'ban'
    ];

    protected $attributes = [
        'verify' => 0,
        'ban' => false
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

}
