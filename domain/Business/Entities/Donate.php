<?php

namespace App\Domain\Business\Entities;

use App\Domain\Account\Entities\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donate extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [

        'user_id',
        'model_type',
        'model_id',
        'description',
        'price',
    ];
    protected $table = 'donate';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function model()
    {
        return $this->morphTo();
    }

}
