<?php

namespace App\Domain\Business\Repositories\Eloquent;

use App\Domain\Business\Contracts\DonateRepository as DonateRepositoryInterface;
use App\Domain\Business\Entities\Donate;
use App\Infrastructure\Repositories\EloquentRepository;

class DonateRepository extends EloquentRepository implements DonateRepositoryInterface
{
    public function entity()
    {
        return Donate::class;
    }
}
