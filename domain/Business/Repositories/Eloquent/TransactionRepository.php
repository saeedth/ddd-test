<?php

namespace App\Domain\Business\Repositories\Eloquent;

use App\Domain\Business\Contracts\TransactionRepository as TransactionRepositoryInterface;
use App\Domain\Business\Entities\Transaction;
use App\Infrastructure\Repositories\EloquentRepository;

class TransactionRepository extends EloquentRepository implements TransactionRepositoryInterface
{
    public function entity()
    {
        return Transaction::class;
    }
}
