<?php

namespace App\Domain\Business\Repositories\Eloquent;

use App\Domain\Business\Contracts\AdvisorRepository as AdvisorRepositoryInterface;
use App\Domain\Business\Entities\Advisor;
use App\Infrastructure\Repositories\EloquentRepository;

class AdvisorRepository extends EloquentRepository implements AdvisorRepositoryInterface
{
    public function entity()
    {
        return Advisor::class;
    }
}
