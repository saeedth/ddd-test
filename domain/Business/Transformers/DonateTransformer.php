<?php

namespace App\Domain\Business\Transformers;

use App\Domain\Business\Entities\Donate;
use App\Domain\Demand\Entities\Request;
use App\Domain\Demand\Transformers\RequestTransformer;
use App\Domain\Supply\Entities\Proposal;
use App\Domain\Supply\Transformers\ProposalTransformer;
use League\Fractal\TransformerAbstract;

class DonateTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user',
        'model'
    ];

    public function transform(Donate $donate)
    {
        return [
            'id' => $donate->id,
            'price' => $donate->price,
            'description' => $donate->description,
        ];
    }

    public function includeUser(Donate $donate)
    {
        $user = $donate->user;
        return $user ? $this->primitive($user, fn($user) => ['id' => $user->id, 'name' => $user->first_name . ' ' . $user->last_name]) : $this->null();
    }

    public function includeModel(Donate $donate)
    {
        $model = $donate->model;
        if ($model instanceof Proposal) {
            return $this->item($model, new ProposalTransformer);
        } elseif ($model instanceof Request) {
            return $this->item($model, new RequestTransformer);
        } else {
            $this->null();
        }
    }
}
