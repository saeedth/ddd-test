<?php

namespace App\Domain\Business\Transformers;

use App\Domain\Business\Entities\Advisor;
use League\Fractal\TransformerAbstract;

class AdvisorTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'user',
        'category',
        'area'
    ];

    public function transform(Advisor $advisor)
    {
        return [
            'id' => $advisor->id,
            'name' => $advisor->name,
            'phone' => $advisor->phone,
            'address' => $advisor->address,
            'description' => $advisor->description,
        ];
    }

    public function includeUser(Advisor $advisor)
    {
        $user = $advisor->user;
        return $user ? ['id' => $user->id, 'name' => $user->name] : $this->null();
    }

    public function includeCategory(Advisor $advisor)
    {
        $category = $advisor->category;
        return $category ? ['id' => $category->id, 'name' => $category->name] : $this->null();
    }

    public function includeArea(Advisor $advisor)
    {
        $area = $advisor->area;
        return $area ? ['id' => $area->id, 'name' => $area->name] : $this->null();
    }
}
