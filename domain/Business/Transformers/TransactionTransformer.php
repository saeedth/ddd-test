<?php

namespace App\Domain\Business\Transformers;

use App\Domain\Business\Entities\Transaction;
use App\Domain\Demand\Entities\Request;
use App\Domain\Demand\Transformers\RequestTransformer;
use App\Domain\Supply\Entities\Proposal;
use App\Domain\Supply\Transformers\ProposalTransformer;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user',
        'model'
    ];

    public function transform(Transaction $transaction)
    {
        return [
            'id' => $transaction->id,
            'uuid' => $transaction->price,
            'user' => $transaction->user->id,
            'model' => $transaction->model,
            'amount' => $transaction->amount,
            'gateway' => $transaction->gate_way,
            'referenceCode' => $transaction->reference_code,
            'authority' => $transaction->authority,
            'status' => $transaction->status,
            'payed_at' => $transaction->payed_at,
        ];
    }

    public function includeUser(Transaction $transaction)
    {
        $user = $transaction->user;
        return $user ? $this->primitive($user, fn($user) => ['id' => $user->id, 'name' => $user->first_name . ' ' . $user->last_name]) : $this->null();
    }

    public function includeModel(Transaction $transaction)
    {
        $model = $transaction->model;
        if ($model instanceof Proposal) {
            return $this->item($model, new ProposalTransformer);
        } elseif ($model instanceof Request) {
            return $this->item($model, new RequestTransformer);
        } else {
            $this->null();
        }
    }
}
