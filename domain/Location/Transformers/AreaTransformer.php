<?php

namespace App\Domain\Location\Transformers;

use App\Domain\Location\Entities\Area;
use League\Fractal\TransformerAbstract;

class AreaTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'city'
    ];

    public function transform(Area $area)
    {
        return [
            'id' => $area->id,
            'name' => $area->name,
            'aliases' => $area->aliases,
            'city_id' => $area->city_id,
        ];
    }

    public function includeCity(Area $area)
    {
        $city = $area->city;
        return $this->primitive($city, fn($city) => ['id' => $city->id, 'name' => $city->name]);

    }
}
