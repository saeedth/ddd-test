<?php

namespace App\Domain\Location\Transformers;

use App\Domain\Location\Entities\Province;
use League\Fractal\TransformerAbstract;

class  ProvinceTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'cities'
    ];

    public function transform(Province $province)
    {
        return [
            'id' => $province->id,
            'name' => $province->name,
            'tell_prefix' => $province->tell_prefix,
            'active' => $province->active,
        ];
    }

    public function includeCities(Province $province)
    {
        $cities = $province->cities;
        return $this->collection($cities, new CityTransformer);
    }
}
