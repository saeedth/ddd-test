<?php

namespace App\Domain\Location\Transformers;

use App\Domain\Location\Entities\City;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'province'
    ];

    public function transform(City $city)
    {
        return [
            'id' => $city->id,
            'name' => $city->name,
            'province_id' => $city->province_id,
            'active' => $city->active
        ];
    }

    public function includeProvince(City $city)
    {
        return $this->primitive($city->province, fn($province) => ['name' => $province->name, 'id' => $province->id, 'tell' => $province->tell_prefix]);

    }
}
