<?php

namespace App\Domain\Location\Repositories\Eloquent;

use App\Domain\Location\Contracts\AreaRepository as AreaRepositoryInterface;
use App\Domain\Location\Entities\Area;
use App\Infrastructure\Repositories\EloquentRepository;

class AreaRepository extends EloquentRepository implements AreaRepositoryInterface
{
    public function entity()
    {
        return Area::class;
    }
}
