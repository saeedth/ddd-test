<?php

namespace App\Domain\Location\Repositories\Eloquent;

use App\Domain\Location\Contracts\ProvinceRepository as ProvinceRepositoryInterface;
use App\Domain\Location\Entities\Province;
use App\Infrastructure\Repositories\EloquentRepository;

class ProvinceRepository extends EloquentRepository implements ProvinceRepositoryInterface
{
    public function entity()
    {
        return Province::class;
    }
}
