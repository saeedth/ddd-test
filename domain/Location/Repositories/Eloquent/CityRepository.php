<?php

namespace App\Domain\Location\Repositories\Eloquent;

use App\Domain\Location\Contracts\CityRepository as CityRepositoryInterface;
use App\Domain\Location\Entities\City;
use App\Infrastructure\Repositories\EloquentRepository;

class CityRepository extends EloquentRepository implements CityRepositoryInterface
{
    public function entity()
    {
        return City::class;
    }
}
