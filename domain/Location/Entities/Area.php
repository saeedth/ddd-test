<?php

namespace App\Domain\Location\Entities;

use App\Domain\Account\Entities\User;
use App\Domain\Business\Entities\Advisor;
use App\Domain\Demand\Entities\Request;
use App\Domain\Supply\Entities\Proposal;
use Database\Factories\AreaFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'aliases',
        'city_id',
    ];

    protected $casts = [
        'aliases' => 'array',
    ];


    public function advisors()
    {
        return $this->hasMany(Advisor::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_has_area');
    }

    public function proposals()
    {
        return $this->belongsTo(Proposal::class);
    }

    public function requests()
    {
        return $this->belongsToMany(Request::class, 'request_has_area');
    }

    protected static function newFactory()
    {
        return new AreaFactory();
    }
}
