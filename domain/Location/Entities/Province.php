<?php

namespace App\Domain\Location\Entities;

use Database\Factories\ProvinceFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'tell_prefix',
        'active',
        'id',
    ];

    protected $attributes = [
        'active' => 0
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    protected static function newFactory()
    {
        return new ProvinceFactory();
    }
}
