<?php

namespace App\Domain\Location\Entities;

use App\Domain\Account\Entities\User;
use Database\Factories\CityFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'province_id',
        'active',
        'id'
    ];
    protected $attributes = [
        'active' => 0
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    protected static function newFactory()
    {
        return new CityFactory();
    }
}
