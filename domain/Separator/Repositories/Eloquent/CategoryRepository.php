<?php

namespace App\Domain\Separator\Repositories\Eloquent;

use App\Domain\Separator\Contracts\CategoryRepository as CategoryRepositoryInterface;
use App\Domain\Separator\Entities\Category;
use App\Infrastructure\Repositories\EloquentRepository;

class CategoryRepository extends EloquentRepository implements CategoryRepositoryInterface
{
    public function entity()
    {
        return Category::class;
    }
}
