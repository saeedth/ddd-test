<?php

namespace App\Domain\Separator\Repositories\Eloquent;

use App\Domain\Separator\Contracts\UnitRepository as UnitRepositoryInterface;
use App\Domain\Separator\Entities\Unit;
use App\Infrastructure\Repositories\EloquentRepository;

class UnitRepository extends EloquentRepository implements UnitRepositoryInterface
{
    public function entity()
    {
        return Unit::class;
    }
}
