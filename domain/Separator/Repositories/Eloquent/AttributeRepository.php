<?php

namespace App\Domain\Separator\Repositories\Eloquent;

use App\Domain\Separator\Contracts\AttributeRepository as AttributeRepositoryInterface;
use App\Domain\Separator\Entities\Attribute;
use App\Infrastructure\Repositories\EloquentRepository;

class AttributeRepository extends EloquentRepository implements AttributeRepositoryInterface
{
    public function entity()
    {
        return Attribute::class;
    }
}
