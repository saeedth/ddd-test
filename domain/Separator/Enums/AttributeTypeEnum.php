<?php

namespace App\Domain\Separator\Enums;

use App\Infrastructure\Contracts\Enums\LocalizeFaDescription;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;


final class AttributeTypeEnum extends Enum implements LocalizedEnum,LocalizeFaDescription
{
    public const NUMBERS = 1;
    public const PERIODS = 2;
    public const SWITCH = 3;


    public static function getLocalizeFaDescription(): array
    {
        return [
            self::class => [
                'NUMBERS' => 'عددی',
                'SWITCH' => 'گزینه ای',
                'PERIODS' => 'دوره ای'
            ]
        ];
    }
}
