<?php

namespace App\Domain\Separator\Entities;

use App\Domain\Demand\Entities\Request;
use App\Domain\Supply\Entities\Proposal;
use Database\Factories\AttributeFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{

    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'category_id',
        'sort',
        'active',
        'type',
        'filter',
        'unit_id',
        'id',
        'range'
    ];


    protected $attributes = [
        'active' => false,
        'filter' => false,
        'special' => false
    ];


    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function periods()
    {
        return $this->belongsToMany(Request::class, 'request_has_period')
            ->withPivot(['from', 'to']);
    }

    public function booleans()
    {

        return $this->belongsToMany(Request::class, 'request_has_boolean')
            ->withPivot(['checked']);
    }

    public function numbers()
    {
        return $this->belongsToMany(Request::class, 'request_has_number')
            ->withPivot(['number']);
    }

    public function proposals()
    {
        return $this->belongsToMany(Proposal::class, 'proposal_has_attribute')
            ->withPivot(['value']);
    }

    public function scopeSort(Builder $query, $value)
    {

        return $query->where('sort', '>=', $value);
    }

    public function scopeCategory(Builder $query, $value)
    {
        return $query->join('categories', 'categories.id', '=', 'attributes.category_id')
            ->where('categories.name', 'like', "%$value%")
            ->select('attributes.*');
    }

    protected static function newFactory()
    {
        return new AttributeFactory();
    }

}
