<?php

namespace App\Domain\Separator\Entities;

use Database\Factories\UnitFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'persian_name',
        'english_name',
        'active',
        'id'
    ];

    protected $attributes = [
        'active' => 0
    ];


    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

    protected static function newFactory()
    {
        return new UnitFactory();
    }
}
