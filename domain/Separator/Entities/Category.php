<?php

namespace App\Domain\Separator\Entities;

use App\Domain\Business\Entities\Advisor;
use App\Domain\Demand\Entities\Request;
use App\Domain\Supply\Entities\Proposal;
use Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'active',
        'description',
        'id'
    ];

    protected $attributes = [
        'active' => 0
    ];

    public function advisors()
    {
        return $this->hasMany(Advisor::class);
    }


    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function proposals()
    {
        return $this->belongsTo(Proposal::class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

    protected static function newFactory()
    {
        return new CategoryFactory();
    }
}
