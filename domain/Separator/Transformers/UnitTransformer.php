<?php

namespace App\Domain\Separator\Transformers;

use App\Domain\Separator\Entities\Unit;
use League\Fractal\TransformerAbstract;

class UnitTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'attributes'
    ];

    public function transform(Unit $unit)
    {
        return [
            'id' => $unit->id,
            'english_name' => $unit->english_name,
            'persian_name' => $unit->persian_name,
            'active' => $unit->active,
        ];
    }

    public function IncludeAttributes(Unit $unit)
    {
        $attributes = $unit->attributes;
        return $this->collection($attributes, new AttributeTransformer);
    }
}
