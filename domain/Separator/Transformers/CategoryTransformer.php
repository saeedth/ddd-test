<?php

namespace App\Domain\Separator\Transformers;

use App\Domain\Separator\Entities\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'attributes'
    ];


    public function includeAttributes(Category $category)
    {

        $attributes = $category->attributes;

        $newAttributes = [];
        foreach ($attributes as $attribute) {
            $newAttributes[] = ['id' => $attribute->id, 'name' => $attribute->name, 'type' => $attribute->type];
        }

        return $this->primitive($newAttributes);
    }

    public function transform(Category $category)
    {
        return [
            'name' => $category->name,
            'active' => $category->active,
            'description' => $category->description,
            'id' => $category->id
        ];
    }


}
