<?php

namespace App\Domain\Separator\Transformers;


use App\Domain\Separator\Entities\Attribute;
use App\Domain\Separator\Enums\AttributeTypeEnum;
use League\Fractal\TransformerAbstract;

class AttributeTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'category',
        'unit'
    ];

    public function transform(Attribute $attribute)
    {
        return [
            'id' => $attribute->id,
            'name' => $attribute->name,
            'sort' => $attribute->sort,
            'active' => $attribute->active,
            'type' => AttributeTypeEnum::getDescription(AttributeTypeEnum::getKey($attribute->type)),
            'type_id' => $attribute->type,
            'filter' => $attribute->filter,
            'range' => $attribute->range
        ];
    }

    public function includeCategory(Attribute $attribute)
    {

        if (!$attribute->category) {
            return $this->null();
        }
        return $this->primitive($attribute->category, fn($category) => ['id' => $category->id, 'name' => $category->name]);
    }

    public function includeUnit(Attribute $attribute)
    {
        if (!$attribute->unit) {
            return $this->null();
        }
        return $this->primitive($attribute->unit, fn($unit) => ['id' => $unit->id, 'name' => $unit->persian_name]);
    }
}
