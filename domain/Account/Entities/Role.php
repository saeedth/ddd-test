<?php

namespace App\Domain\Account\Entities;

use Database\Factories\RoleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = [
        'name',
        'guard_name',
        'editable',
        'id'
    ];

    protected $hidden = [
        'pivot'
    ];

    protected static function newFactory()
    {
        return new RoleFactory();
    }
}
