<?php

namespace App\Domain\Account\Entities;

use App\Domain\Business\Entities\Advisor;
use App\Domain\Demand\Entities\Request;
use App\Domain\Location\Entities\Area;
use App\Domain\Location\Entities\City;
use App\Domain\Supply\Entities\Proposal;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasApiTokens;
    use InteractsWithMedia;
    use HasRoles;
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'mobile',
        'first_name',
        'last_name',
        'type',
        'city_id',
        "id",
        "gender"
    ];

    protected $attributes = [
        'type' => 0,
        'gender' => 0

    ];

    protected $hidden = [
        'remember_token',
    ];


    public function advisor()
    {
        return $this->hasOne(Advisor::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class, 'user_has_area');
    }

    /**
     * A model may have multiple roles.
     */
    public function roles(): BelongsToMany
    {
        return $this->morphToMany(
            Role::class,
            'model',
            'model_has_roles',
            'model_id',
            'role_id'
        );
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
            ->singleFile();
    }


    public function scopeAreaId(Builder $query, $value)
    {
        return $query->join('user_has_area as uha', 'uha.user_id', 'users.id')->where('uha.area_id', $value);
    }

    public function scopeRoleId(Builder $query, $value)
    {
        return $query->join('model_has_roles as mhr', 'mhr.model_id', 'users.id')
            ->where('mhr.role_id', $value);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory()
    {
        return new UserFactory();
    }
}
