<?php

namespace App\Domain\Account\Repositories\Eloquent;

use App\Domain\Account\Contracts\PersonalAccessTokenRepository as ContractsPersonalAccessTokenRepository;
use App\Infrastructure\Repositories\EloquentRepository;
use Laravel\Sanctum\Sanctum;

class PersonalAccessTokenRepository extends EloquentRepository implements ContractsPersonalAccessTokenRepository
{
    public function entity()
    {
        return Sanctum::$personalAccessTokenModel;
    }
}
