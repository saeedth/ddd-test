<?php

namespace App\Domain\Account\Repositories\Eloquent;

use App\Domain\Account\Contracts\RoleRepository as RoleRepositoryInterface;
use App\Domain\Account\Entities\Role;
use App\Infrastructure\Repositories\EloquentRepository;
use Database\Seeders\Permissions;
use Illuminate\Support\Arr;

class RoleRepository extends EloquentRepository implements RoleRepositoryInterface
{
    public function entity()
    {
        return Role::class;
    }

    public function create($roleData)
    {
        $role = Arr::except($roleData, 'permissions');
        $permissions = $roleData['permissions'];

        $roleRecord = $this->entity->create($role);
        $roleRecord->permissions()->attach($permissions);

    }

    public function update($roleId, $roleData)
    {
        $permissions = $roleData['permissions'];
        $role = $this->find($roleId);
        $role->update(Arr::except($roleData, 'permissions'));
        return $role->permissions()->sync($permissions);


    }
}
