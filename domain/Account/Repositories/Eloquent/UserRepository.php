<?php

namespace App\Domain\Account\Repositories\Eloquent;

use App\Domain\Account\Contracts\UserRepository as UserRepositoryInterface;
use App\Domain\Account\Entities\User;
use App\Infrastructure\Repositories\EloquentRepository;
use Illuminate\Support\Arr;


class UserRepository extends EloquentRepository implements UserRepositoryInterface
{
    public function entity()
    {
        return User::class;
    }

    public function loginUser($guard = null)
    {
        return auth($guard)->user();
    }


    public function create($userData)
    {

        $user = $this->entity->create(Arr::except($userData, 'areas'));
        $user->roles()->attach($userData['roles']);
        return $user->areas()->attach($userData['areas']);
    }

    public function update($userId, $properties)
    {

        $user = $this->find($userId);
        $user->update(Arr::except($properties, ['areas', 'roles']));
        $user->roles()->sync($properties['roles']);
        return $user->areas()->sync($properties['areas']);

    }


    public function editProfile($userId, $userData)
    {

        $user = $this->find($userId);

        return $user->update(Arr::except($userData, ['id']));

    }
}
