<?php

namespace App\Domain\Account\Repositories\Eloquent;

use App\Domain\Account\Contracts\PermissionRepository as PermissionRepositoryInterface;
use App\Domain\Account\Entities\Permission;
use App\Infrastructure\Repositories\EloquentRepository;

class PermissionRepository extends EloquentRepository implements PermissionRepositoryInterface
{
    public function entity()
    {
        return Permission::class;
    }
}
