<?php

namespace App\Domain\Account\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static UNKNOWN()
 * @method static static MALE()
 * @method static static FEMALE()
 */
final class UserGenderTypeEnum extends Enum
{
    const UNKNOWN = 0;
    const MALE = 1;
    const FEMALE = 2;
}
