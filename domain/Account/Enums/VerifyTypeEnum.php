<?php

namespace App\Domain\Account\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PENDING()
 * @method static static ACCEPT()
 * @method static static REJECT()
 */
final class VerifyTypeEnum extends Enum
{
    const PENDING = 0;
    const ACCEPT = 1;
    const REJECT = 2;
}
