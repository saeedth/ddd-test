<?php

namespace App\Domain\Account\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static NORMAL()
 * @method static static ADVISOR()
 */
final class UserTypeEnum extends Enum
{
    const NORMAL = 0;
    const ADVISOR = 1;
}
