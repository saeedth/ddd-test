<?php

namespace App\Domain\Account\Contracts;

interface UserRepository
{
    public function loginUser($guard);

    public function update($userId, $properties);
}
