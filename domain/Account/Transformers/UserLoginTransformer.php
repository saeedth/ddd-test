<?php

namespace App\Domain\Account\Transformers;

use App\Domain\Account\Contracts\UserRepository;
use App\Domain\Account\Entities\User;
use League\Fractal\TransformerAbstract;

class UserLoginTransformer extends TransformerAbstract
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'type' => $user->type,
            'gender' => $user->gender,
            'first_name' => $user->first_name,
            'city_id' => $user->city_id,
            'mobile' => $user->mobile,
            'last_name' => $user->last_name,
            'avatar' => $user->getFirstMediaUrl('avatar') ?? null,
            'role-permission' => $this->rolePermissions($user)
        ];
    }

    private function rolePermissions($user)
    {

        $roles = $user->roles;
        $role_permissions = [];
        foreach ($roles as $role) {
            $role_permissions[] = [$role->name => $role->permissions->map(fn($permission) => ['id' => $permission->id, 'name' => $permission->name])];
        }

        return $role_permissions;
    }
}
