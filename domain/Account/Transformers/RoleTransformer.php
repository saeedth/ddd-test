<?php

namespace App\Domain\Account\Transformers;

use App\Domain\Account\Entities\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'permissions'
    ];


    public function transform(Role $role)
    {
        return
            [
                'id' => $role->id,
                'name' => $role->name,
                'editable' => $role->editable
            ];
    }

    public function includePermissions(Role $role)
    {
        $permissions = $role->permissions;
        return $this->collection($permissions, new PermissionTransformer);
    }
}
