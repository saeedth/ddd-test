<?php

namespace App\Domain\Account\Transformers;

use App\Domain\Account\Entities\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{


    protected $availableIncludes = [
        'city', 'roles', 'areas'
    ];


    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'first_name' => $user->first_name ?: 'کاربر غزیز',
            'mobile' => $user->mobile,
            'last_name' => $user->last_name,
            'city_name' => $user->city->name ?? null,
            'city_id' => $user->city_id,
            'avatar' => $user->getFirstMediaUrl('avatar'),
            'type' => $user->type,
            'gender' => $user->gender
        ];
    }

    public function includeCity(User $user)
    {

        $city = $user->city;
        return $city ? $this->primitive($city, fn($city) => ['id' => $city->id, 'name' => $city->name]) : $this->null();

    }

    public function includeRoles(User $user)
    {
        $roles = $user->roles->pluck('id');
        return $this->primitive($roles);

    }


    public function includeAreas(User $user)
    {

        $areas = $user->areas->pluck('id');

        return $this->primitive($areas);
    }

}
