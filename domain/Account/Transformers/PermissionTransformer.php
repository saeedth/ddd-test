<?php

namespace App\Domain\Account\Transformers;

use App\Domain\Account\Entities\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'roles'
    ];

    public function transform(Permission $permission)
    {
        return [
            'id' => $permission->id,
            'name' => explode('.', $permission->name)[1] . '.' . explode('.', $permission->name)[2],
        ];
    }

    public function includeRoles(Permission $permission)
    {
        $roles = $permission->roles;
        return $roles ? $this->collection($roles, new RoleTransformer) : $this->null();
    }
}
