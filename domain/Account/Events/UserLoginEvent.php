<?php

namespace App\Domain\Account\Events;

use Illuminate\Queue\SerializesModels;

class UserLoginEvent
{
    use SerializesModels;

    public $user;
    public $code;

    public function __construct($user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }
}
