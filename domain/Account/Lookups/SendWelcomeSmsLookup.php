<?php

namespace App\Domain\Account\Lookups;

use App\Infrastructure\Lookups\LookupChannel;
use Illuminate\Notifications\Notification;

class SendWelcomeSmsLookup extends LookupChannel
{
    protected function template(): string
    {
        return "yareaval-welcome-login";
    }

    protected function token10($notifiable, Notification $notification): string
    {
        return $notifiable->first_name ?? 'کاربر';
    }

    protected function token($notifiable, Notification $notification): string
    {
        return $notification->timeWhenLogin;

    }
}
