<?php

namespace App\Domain\Account\Lookups;

use App\Infrastructure\Lookups\LookupChannel;
use Illuminate\Notifications\Notification;

class SendOTPCodeLookup extends LookupChannel
{
    protected function template(): string
    {
        return 'yareaval-otp-login';
    }

    protected function token($notifiable, Notification $notification): string
    {
        return $notification->code;
    }
}
