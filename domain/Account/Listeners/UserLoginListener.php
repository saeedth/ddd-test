<?php

namespace App\Domain\Account\Listeners;

use App\Domain\Account\Events\UserLoginEvent;
use App\Domain\Account\Notifications\UserLoginNotification;

class UserLoginListener
{
    public function handle(UserLoginEvent $event)
    {
        $event->user->notify(new UserLoginNotification($event->code));
    }
}
