<?php

namespace App\Domain\Account\Listeners;

use App\Domain\Account\Events\WelcomeToUser;
use App\Domain\Account\Notifications\WelcomeToUserNotification;

class WelcomeToUserListener
{
    /**
     * Handle the event.
     *
     * @param WelcomeToUser $event
     * @return void
     */
    public function handle(WelcomeToUser $event)
    {
        $event->user->notify(new WelcomeToUserNotification());
    }

}
