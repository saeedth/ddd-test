<?php

namespace App\Domain\Account\Notifications;

use App\Domain\Account\Lookups\SendWelcomeSmsLookup;
use Hekmatinasser\Verta\Verta;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class WelcomeToUserNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $timeWhenLogin;

    public function __construct()
    {
        $this->timeWhenLogin = $this->timeWhenLogin();

    }

    public function via($notifiable)
    {
        return [SendWelcomeSmsLookup::class];

    }

    private function timeWhenLogin():Verta
    {
        return now()->toJalali();
    }
}
