<?php

namespace App\Domain\Demand\Contracts;

interface RequestRepository
{

    public function create($requestData);


    public function update($requestId, $requestData);


}
