<?php

namespace App\Domain\Demand\Events;

use Illuminate\Queue\SerializesModels;

class SendProposalLikeRequestEvent
{
    use SerializesModels;

    public $user;
    public $proposalCount;
    public $requestId;


    public function __construct($user, $requestId, $proposalCount)
    {
        $this->user = $user;
        $this->proposalCount = $proposalCount;
        $this->requestId = $requestId;
    }
}
