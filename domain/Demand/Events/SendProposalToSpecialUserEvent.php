<?php

namespace App\Domain\Demand\Events;

use Illuminate\Queue\SerializesModels;

class SendProposalToSpecialUserEvent
{
    use SerializesModels;

    public $user;
    public $proposalId;
    public $attributes = '';


    public function __construct($user, $proposalId, $attributes)
    {
        $this->user = $user;
        $this->proposalId = $proposalId;
        $this->attributes = $this->concatAttributeItemToString($attributes);
    }

    private function concatAttributeItemToString($attributes)
    {

        $concatAttributes = '';

        foreach ($attributes as $attribute) {
            $concatAttributes .= "${attribute['name']} : ${attribute['value']}, ";
        }

        return rtrim($concatAttributes, ', ');
    }


}
