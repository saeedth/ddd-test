<?php

namespace App\Domain\Demand\Events;

use Illuminate\Queue\SerializesModels;

class SendRequestTrackingCodeToUserEvent
{
    use SerializesModels;

    public $user;
    public $trackingCode;

    public function __construct($user, $trackingCode)
    {
        $this->trackingCode = $trackingCode;
        $this->user = $user;
    }
}
