<?php

namespace App\Domain\Demand\Repositories\Eloquent;

use App\Domain\Demand\Contracts\RequestRepository as RequestRepositoryInterface;
use App\Domain\Demand\Entities\Request;
use App\Domain\Demand\Strategy\Request\Concrete\RequestBoolean;
use App\Domain\Demand\Strategy\Request\Concrete\RequestNumber;
use App\Domain\Demand\Strategy\Request\Concrete\RequestPeriod;
use App\Domain\Demand\Strategy\Request\Manager\StoreManager;
use App\Infrastructure\Contracts\Request\StoreStrategy as StoreStrategyInterface;
use App\Infrastructure\Repositories\EloquentRepository;
use Illuminate\Support\Arr;

class RequestRepository extends EloquentRepository implements RequestRepositoryInterface
{
    public function entity()
    {
        return Request::class;
    }


    public function create($requestData)
    {

        $request = $this->entity->create(Arr::only($requestData, ['category_id', 'user_id', 'description', 'tracking_code']));

        $areas = $requestData['areas'];

        $request->areas()->attach($areas);

        $this->requestAttributeAttach($requestData['attributes'], $request);

        return $request;
    }


    public function update($requestId, $requestData)
    {

        $request = $this->find($requestId);
        $request->update(Arr::only($requestData, ['category_id', 'user_id', 'description']));

        $request->areas()->sync($requestData['areas']);

        $this->requestAttributeSync($requestData['attributes'], $request);
    }


    private function requestAttributeAttach($attributes, $request)
    {
        foreach ($attributes as $key => $value) {

            if ($key == 'number') {
                app()->bind(StoreStrategyInterface::class, RequestNumber::class);
            }
            if ($key == 'period') {
                app()->bind(StoreStrategyInterface::class, RequestPeriod::class);
            }
            if ($key == 'switch') {
                app()->bind(StoreStrategyInterface::class, RequestBoolean::class);
            }
            $data = resolve(StoreManager::class)->store($value);

            $request->{$data['relation']}()->attach(Arr::except($data, 'relation'));
        }
    }

    private function requestAttributeSync($attributes, $request)
    {
        foreach ($attributes as $key => $value) {

            if ($key == 'number') {
                app()->bind(StoreStrategyInterface::class, RequestNumber::class);
            }
            if ($key == 'period') {
                app()->bind(StoreStrategyInterface::class, RequestPeriod::class);
            }
            if ($key == 'switch') {
                app()->bind(StoreStrategyInterface::class, RequestBoolean::class);
            }
            $data = resolve(StoreManager::class)->store($value);

            $request->{$data['relation']}()->sync(Arr::except($data, 'relation'));
        }
    }

    // when sending proposal is automatic
    public function saveProposalToRequest($request, $proposalIds)
    {
        $request->proposals()->attach($proposalIds);
    }

    // when sendig proposal in manuall
    public function assignProposalsToSpecialRequest(int $requestId, $proposals)
    {
        $request = $this->entity->find($requestId);
        $request->proposals()->attach($proposals);
        return $request;
    }
}
