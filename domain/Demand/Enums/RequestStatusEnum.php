<?php

namespace App\Domain\Demand\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static normal()
 * @method static static realEstate()
 */
final class RequestStatusEnum extends Enum
{
    public const SALE = 1;
    public const RENT = 2;
    public const MORTGAGE = 3;
}
