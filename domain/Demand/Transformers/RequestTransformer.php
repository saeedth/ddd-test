<?php

namespace App\Domain\Demand\Transformers;

use App\Domain\Demand\Entities\Request;
use App\Domain\Supply\Transformers\ProposalTransformer;
use League\Fractal\TransformerAbstract;

class RequestTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'category',
        'user',
        'areas',
        'proposals',
        'attributes'
    ];

    public function transform(Request $request)
    {
        return [
            'id' => $request->id,
            'user_id' => $request->user_id,
            'status_id' => $request->status,
            'description' => $request->description
        ];
    }

    public function includeAttributes(Request $request)
    {
        $booleanTypes = $request->booleans;
        $numberTypes = $request->numbers;
        $periodsTypes = $request->periods;
        $attributes = [];
        foreach ($booleanTypes as $type) {
            $attributes['switch'][] = ['id' => $type->id, 'name' => $type->name, 'value' => $type->pivot->checked];
        }
        foreach ($numberTypes as $type) {
            $attributes['number'][] = ['id' => $type->id, 'name' => $type->name, 'value' => $type->pivot->number];
        }
        foreach ($periodsTypes as $type) {
            $attributes['period'][] = ['id' => $type->id, 'name' => $type->name, 'from' => $type->pivot->from, 'to' => $type->pivot->to];
        }

        return $this->primitive($attributes);
    }

    public function includeCategory(Request $request)
    {
        $category = $request->category;
        return $category ? $this->primitive($category, fn($category) => ['id' => $category->id, 'name' => $category->name]) : $this->null();

    }

    public function includeUser(Request $request)
    {
        $user = $request->user;
        return $user ? $this->primitive($user, fn($user) => ['id' => $user->id, 'name' => $user->first_name]) : $this->null();
    }

    public function includeAreas(Request $request)
    {
        $areas = $request->areas->pluck('id');
        return $areas ? $this->primitive($areas) : $this->null();

    }


    public function includeProposals(Request $request)
    {
        $proposals = $request->proposals;
        return $this->collection($proposals, new ProposalTransformer);
    }
}
