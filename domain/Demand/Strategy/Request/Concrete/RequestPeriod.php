<?php

namespace App\Domain\Demand\Strategy\Request\Concrete;


use App\Infrastructure\Contracts\Request\StoreStrategy;


class RequestPeriod extends StoreStrategy
{

    protected $relationMethod = 'periods';

    public function store($attributes)
    {
        $newAttributeData = [];
        foreach ($attributes as $key => $value) {

            $newAttributeData[$value['id']] = [

                'from' => $value['from'],
                'to' => $value['to']
            ];
        }

        $newAttributeData['relation'] = $this->relationMethod;

        return $newAttributeData;
    }
}
