<?php

namespace App\Domain\Demand\Strategy\Request\Concrete;


use App\Infrastructure\Contracts\Request\StoreStrategy;


class RequestBoolean extends StoreStrategy
{

    protected $relationMethod = 'booleans';

    public function store($attributes)
    {
        $newAttributeData = [];
        foreach ($attributes as $key => $value) {

            $newAttributeData[$value['id']] = [
                'checked' => $value['value']
            ];
        }
        $newAttributeData['relation'] = $this->relationMethod;

        return $newAttributeData;

    }
}
