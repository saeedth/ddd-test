<?php

namespace App\Domain\Demand\Strategy\Request\Concrete;


use App\Infrastructure\Contracts\Request\StoreStrategy;


class RequestNumber extends StoreStrategy
{

    protected $relationMethod = 'numbers';

    public function store($attributes)
    {
        $newAttributeData = [];
        foreach ($attributes as $key => $value) {

            $newAttributeData[$value['id']] = [
                'number' => $value['value']
            ];
        }
        $newAttributeData['relation'] = $this->relationMethod;
        return $newAttributeData;

    }
}
