<?php


namespace App\Domain\Demand\Strategy\Request\Manager;


use App\Infrastructure\Contracts\Request\StoreStrategy;


class StoreManager
{

    protected $store;

    public function __construct()
    {
        $this->store = resolve(StoreStrategy::class);
    }

    public function store(array $requestData): array
    {
        return $this->store->store($requestData);
    }
}
