<?php

namespace App\Domain\Demand\Listeners;

use App\Domain\Demand\Events\SendProposalToSpecialUserEvent;
use App\Domain\Demand\Notifications\SendProposalToSpecialUserNotification;

class SendProposalToSpecialUserListener
{
    public function handle(SendProposalToSpecialUserEvent $event)
    {
        $event->user->notify(new SendProposalToSpecialUserNotification($event->proposalId, $event->attributes));
    }
}
