<?php

namespace App\Domain\Demand\Listeners;

use App\Domain\Demand\Events\SendProposalLikeRequestEvent;
use App\Domain\Demand\Notifications\SendProposalsLikeRequestNotification;

class SendProposalLikeRequestListener
{
    public function handle(SendProposalLikeRequestEvent $event)
    {
        $event->user->notify(new SendProposalsLikeRequestNotification($event->requestId, $event->proposalCount));
    }
}
