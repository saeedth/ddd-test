<?php

namespace App\Domain\Demand\Listeners;

use App\Domain\Demand\Events\SendRequestTrackingCodeToUserEvent;
use App\Domain\Demand\Notifications\RequestTrackingCodeNotification;

class SendRequestTrackingCodeToUserListener
{
    public function handle(SendRequestTrackingCodeToUserEvent $event)
    {
        $event->user->notify(new RequestTrackingCodeNotification($event->trackingCode));
    }
}
