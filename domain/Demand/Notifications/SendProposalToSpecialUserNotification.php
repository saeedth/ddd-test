<?php

namespace App\Domain\Demand\Notifications;

use App\Domain\Demand\Lookups\SendProposalToSpecialUserLookup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class SendProposalToSpecialUserNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $proposalId;
    public $attributes;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct($proposalId, $attributes)
    {
        $this->proposalId = $proposalId;
        $this->attributes = $attributes;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [SendProposalToSpecialUserLookup::class];
    }

}
