<?php

namespace App\Domain\Demand\Notifications;

use App\Domain\Demand\Lookups\SendTrackingCodeLookup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class RequestTrackingCodeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $trackingCode;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct($trackingCode)
    {
        $this->trackingCode = $trackingCode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [SendTrackingCodeLookup::class];
    }

}
