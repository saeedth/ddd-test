<?php

namespace App\Domain\Demand\Notifications;

use App\Domain\Demand\Lookups\SendProposalLikeRequestMessageLookup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class SendProposalsLikeRequestNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $proposalCount;
    public $requestId;


    /**
     * Create a new notification instance.
     *
     */
    public function __construct($requestId, $proposalCount)
    {
        $this->proposalCount = $proposalCount;
        $this->requestId = $requestId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [SendProposalLikeRequestMessageLookup::class];
    }

}
