<?php

namespace App\Domain\Demand\Entities;

use App\Domain\Account\Entities\User;
use App\Domain\Location\Entities\Area;
use App\Domain\Seprator\Entities\Attribute;
use App\Domain\Seprator\Entities\Category;
use App\Domain\Supply\Entities\Proposal;
use Database\Factories\RequestFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'category_id',
        'user_id',
        'status',
        'description',
        'tracking_code'
    ];

    protected $attributes = [
        'status' => 1
    ];


    public function donate()
    {
        return $this->morphOne(Donate::class, 'model');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class, 'request_has_area');
    }

    public function periods()
    {
        return $this->belongsToMany(Attribute::class, 'request_has_period')
            ->withPivot(['from', 'to']);
    }

    public function booleans()
    {
        return $this->belongsToMany(Attribute::class, 'request_has_boolean')
            ->withPivot(['checked']);
    }

    public function numbers()
    {
        return $this->belongsToMany(Attribute::class, 'request_has_numbers')
            ->withPivot(['number']);
    }

    public function proposals()
    {
        return $this->belongsToMany(Proposal::class, 'request_has_proposal');
    }


    public function scopeCategory(Builder $query, $categoryId)
    {
        return $query->where('category_id', $categoryId);
    }

    public function scopeProposal(Builder $query, $proposalId)
    {
        return $query->whereHas('proposals', fn($query) => $query->where('proposal_id', $proposalId));
    }

    public function scopeArea(Builder $query, $areaId)
    {

        return $query->whereHas('areas', fn($query) => $query->where('area_id', $areaId));

    }

    public function scopePeriod(Builder $query, $period)
    {

        return $query->whereHas('periods', function ($query) use ($period) {

            $query->select('id')
                ->where('attribute_id', $period['id'])
                ->where('from', '<=', $period['value'] + $period['range'])
                ->where('to', '>=', $period['value'] - $period['range']);
        });

    }

    public function scopeNumber(Builder $query, $number)
    {

        return $query->whereHas('numbers', function (Builder $query) use ($number) {
            $query->select('id')
                ->where('attribute_id', $number['id'])
                ->where('number', "<=", $number['value'] + $number['range'])
                ->where('number', ">=", $number['value'] - $number['range']);
        });
    }

    public function scopeMobile(Builder $query, $mobile)
    {
        return $query->whereHas('user', fn($query) => $query->where('mobile', $mobile));
    }

    protected static function newFactory()
    {
        return new RequestFactory();
    }
}
