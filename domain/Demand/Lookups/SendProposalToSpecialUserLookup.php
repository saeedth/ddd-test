<?php

namespace App\Domain\Demand\Lookups;

use App\Infrastructure\Lookups\LookupChannel;
use Illuminate\Notifications\Notification;

class SendProposalToSpecialUserLookup extends LookupChannel
{
    protected function template(): string
    {
        return 'show-proposal';
    }

    protected function token($notifiable, Notification $notification): string
    {
        return route('proposal.show', ['id' => $notification->proposalId]);

    }

    protected function token2($notifiable, Notification $notification): string
    {
        return str_replace(' ', '', $notification->attributes);

    }

}
