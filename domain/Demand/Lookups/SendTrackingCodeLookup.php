<?php

namespace App\Domain\Demand\Lookups;

use App\Infrastructure\Lookups\LookupChannel;
use Illuminate\Notifications\Notification;

class SendTrackingCodeLookup extends LookupChannel
{
    protected function template(): string
    {
        return 'yareaval-request-tracking';
    }

    protected function token($notifiable, Notification $notification): string
    {
        return str_replace('trackingCode', $notification->trackingCode, config('trackrequest.track-request'));
    }
}
