<?php

namespace App\Domain\Demand\Lookups;

use App\Infrastructure\Lookups\LookupChannel;
use Illuminate\Notifications\Notification;

class SendProposalLikeRequestMessageLookup extends LookupChannel
{
    protected function template(): string
    {
        return 'yareaval-proposal-related-request';
    }

    protected function token($notifiable, Notification $notification): string
    {
        return $notification->proposalCount;
    }

    protected function token10($notifiable, Notification $notification): string
    {
        return route('requests.show', ['id' => $notification->requestId, 'include' => 'proposals.attributes,attributes']);
    }
}
