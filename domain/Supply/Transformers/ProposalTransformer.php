<?php

namespace App\Domain\Supply\Transformers;

use App\Domain\Supply\Entities\Proposal;
use League\Fractal\TransformerAbstract;
use Morilog\Jalali\CalendarUtils;

class ProposalTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'category',
        'user',
        'requests',
        'attributes',
        'area',
    ];

    public function transform(Proposal $proposal)
    {
        return [
            'id' => $proposal->id,
            'title' => $proposal->title,
            'description' => $proposal->description,
            'status' => $proposal->status,
            'dateTime' => $this->convertToPersianDateTime($proposal->created_at),
            'gallery' => $this->getProposalMedias($proposal)
        ];
    }

    public function getProposalMedias($proposal)
    {
        $mediaUrls = [];
        foreach ($proposal->getMedia('proposal') as $media) {
            $mediaUrls[] = $media->getUrl();
        }
        return $mediaUrls;
    }


    public function IncludeUser(Proposal $proposal)
    {
        $user = $proposal->user;
        return $this->primitive($user, fn($user) => ['fullname' => $user->first_name . ' ' . $user->last_name, 'mobile' => $user->mobile]);

    }

    public function IncludeCategory(Proposal $proposal)
    {
        $category = $proposal->category;
        return $this->primitive($category, fn($category) => ['id' => $category->id, 'name' => $category->name]);
    }

    public function IncludeRequests(Proposal $proposal)
    {
        $requests = $proposal->requests;
        $newRequests = [];
        if ($requests) {
            foreach ($requests as $request) {
                $newRequests[] = ['id' => $request->id, 'description' => $request->description];
            }
        }
        return $this->primitive($newRequests);
    }


    public function IncludeArea(Proposal $proposal)
    {
        $area = $proposal->area;
        return $area ? $this->primitive($area, fn($area) => ['id' => $area->id, 'name' => $area->name]) : $this->null();
    }


    public function IncludeAttributes(Proposal $proposal)
    {
        $attributes = $proposal->attributes;
        $newAttributes = [];
        if ($attributes) {
            foreach ($attributes as $attribute) {
                $newAttributes[] = ['id' => $attribute->id, 'name' => $attribute->name, 'type' => $attribute->type, 'value' => $attribute->pivot->value];
            }
        }
        return $this->primitive($newAttributes);
    }


    private function convertToPersianDateTime($dateTime)
    {
        return CalendarUtils::strftime('Y/m/d H:i', strtotime($dateTime));
    }
}
