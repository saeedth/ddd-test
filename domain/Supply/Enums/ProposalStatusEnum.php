<?php

namespace App\Domain\Supply\Enums;

use BenSampo\Enum\Enum;


final class ProposalStatusEnum extends Enum
{
    public const SALE = 1;
    public const RENT = 2;
    public const MORTGAGE = 3;

}
