<?php

namespace App\Domain\Supply\Contracts;

interface ProposalRepository
{

    public function create($proposalData);

    public function update($proposalId, $proposalData);
}
