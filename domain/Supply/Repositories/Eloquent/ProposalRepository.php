<?php

namespace App\Domain\Supply\Repositories\Eloquent;

use App\Domain\Supply\Contracts\ProposalRepository as ProposalRepositoryInterface;
use App\Domain\Supply\Entities\Proposal;
use App\Infrastructure\Repositories\EloquentRepository;
use Illuminate\Support\Arr;


class ProposalRepository extends EloquentRepository implements ProposalRepositoryInterface
{
    public function entity()
    {
        return Proposal::class;
    }


    public function create($proposalData)
    {
        $proposal = Arr::except($proposalData, ['attributes', 'requests']);
        $attributes = $proposalData['attributes'];

        $proposal = $this->entity->create($proposal);

        $proposal->attributes()->attach($this->proposalHasAttribute($attributes));

        // return $proposal;

        // request has proposal

        array_key_exists('requests', $proposalData) ? $proposal->requests()->attach($proposalData['requests']) : null;

    }

    public function update($proposalId, $proposalData)
    {
        $proposalUpdateData = Arr::except($proposalData, ['attributes', 'request_id']);


        // get proposal
        $proposal = $this->find($proposalId);

        $attributes = $proposalData['attributes'];
        $requests = $proposalData['requests'];

        // proposal has attribute
        $proposal->update($proposalUpdateData);
        $proposal->attributes()->sync($this->proposalHasAttribute($attributes));

        // request has proposal

        $proposal->requests()->sync($requests);
    }

    private function proposalHasAttribute($attributes)
    {

        $hasAttributes = [];
        foreach ($attributes as $key => $value) {
            $hasAttributes[$value['id']] = ['value' => $value['value']];
        }
        return $hasAttributes;
    }
}
