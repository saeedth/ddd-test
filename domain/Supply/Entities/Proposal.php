<?php

namespace App\Domain\Supply\Entities;

use App\Domain\Account\Entities\User;
use App\Domain\Business\Entities\Donate;
use App\Domain\Demand\Entities\Request;
use App\Domain\Location\Entities\Area;
use App\Domain\Seprator\Entities\Attribute;
use App\Domain\Seprator\Entities\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Proposal extends Model implements HasMedia
{

    use InteractsWithMedia;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'title',
        'description',
        'user_id',
        'area_id',
        'category_id',
        'status',
        'sending_automatic_way'
    ];
    protected $attributes = [
        'status' => 1,
        'sending_automatic_way' => 1
    ];


    public function donate()
    {

        return $this->morphOne(Donate::class, 'model');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function requests()
    {
        return $this->belongsToMany(Request::class, 'request_has_proposal');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'proposal_has_attribute')->withPivot(['value']);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }


    public function scopeSendingAutomaticWay(Builder $query, $value)
    {

        return $query->where('sending_automatic_way', $value);
    }

    public function scopeAreas(Builder $query, $value)
    {

        return $query->whereIn('area_id', $value);
    }

    public function scopeCategory(Builder $query, $value)
    {
        return $query->where('category_id', $value);
    }

    public function scopeAttributeId(Builder $query, $attributeId)
    {
        return $query->whereHas('attributes', fn($query) => $query->where('attribute_id', $attributeId));

        // return $query->join('proposal_has_attribute as pha', 'pha.proposal_id', "proposals.id")->where('pha.attribute_id', $value);
    }

    public function scopeMoreThan(Builder $query, $value)
    {
        return $query->where('pha.value', '>=', (int)$value);
    }


    public function scopeLessThan(Builder $query, $value)
    {
        return $query->where('pha.value', '<=', (int)$value);
    }

    public function scopeBetween(Builder $query, ...$values)
    {
        return $query->whereBetween('pha.value', array_map(fn($value) => (int)$value, $values));
    }


    // filter proposals which wasn't sent to this request

    public function scopeRequest(Builder $query, $value)
    {
        return $query->whereDoesntHave('requests', function (Builder $query) use ($value) {
            $query->Where('request_id', $value);
        });
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('proposal');
    }
}
